/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : e-tracking

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-08-22 11:39:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for data_type
-- ----------------------------
DROP TABLE IF EXISTS `data_type`;
CREATE TABLE `data_type` (
  `datatype_id` int(11) NOT NULL AUTO_INCREMENT,
  `datatype_name` varchar(255) NOT NULL,
  PRIMARY KEY (`datatype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_group_id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  PRIMARY KEY (`dept_id`),
  KEY `fk_department_department_group1_idx` (`dept_group_id`),
  CONSTRAINT `fk_department_department_group1` FOREIGN KEY (`dept_group_id`) REFERENCES `department_group` (`dept_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for department_group
-- ----------------------------
DROP TABLE IF EXISTS `department_group`;
CREATE TABLE `department_group` (
  `dept_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_group_name` varchar(255) NOT NULL,
  PRIMARY KEY (`dept_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi
-- ----------------------------
DROP TABLE IF EXISTS `kpi`;
CREATE TABLE `kpi` (
  `kpi_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `kpi_name` longtext NOT NULL,
  `kpi_remark` longtext,
  `kpi_budgetask` double DEFAULT NULL,
  `kpi_budgetgot` double DEFAULT NULL,
  `kpi_count` varchar(255) DEFAULT NULL,
  `kpi_result` varchar(255) DEFAULT NULL,
  `kpi_comment` longtext,
  `dept_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kpi_id`),
  KEY `fk_Kpi_Plan1_idx` (`plan_id`),
  KEY `fk_Kpi_Target1_idx` (`target_id`),
  KEY `fk_Kpi_Department1_idx` (`dept_id`),
  CONSTRAINT `fk_Kpi_Department1` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`),
  CONSTRAINT `fk_Kpi_Plan1` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`plan_id`),
  CONSTRAINT `fk_Kpi_Target1` FOREIGN KEY (`target_id`) REFERENCES `target` (`target_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kpi_log
-- ----------------------------
DROP TABLE IF EXISTS `kpi_log`;
CREATE TABLE `kpi_log` (
  `kpilog_id` int(11) NOT NULL AUTO_INCREMENT,
  `kpi_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `kpi_name` mediumtext NOT NULL,
  `kpi_remark` mediumtext,
  `kpi_budgetask` double DEFAULT NULL,
  `kpi_budgetgot` double DEFAULT NULL,
  `kpi_count` varchar(255) DEFAULT NULL,
  `kpi_result` varchar(255) DEFAULT NULL,
  `kpi_comment` longtext,
  `dept_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`kpilog_id`),
  KEY `fk_Kpilog_Kpi1_idx` (`kpi_id`),
  KEY `fk_Kpilog_User1_idx` (`user_id`),
  CONSTRAINT `fk_Kpi_Kpi1` FOREIGN KEY (`kpi_id`) REFERENCES `kpi` (`kpi_id`),
  CONSTRAINT `fk_Kpilog_User1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mission
-- ----------------------------
DROP TABLE IF EXISTS `mission`;
CREATE TABLE `mission` (
  `mission_id` int(11) NOT NULL AUTO_INCREMENT,
  `strategy_id` int(11) NOT NULL,
  `mission_name` longtext NOT NULL,
  `mission_remark` longtext,
  PRIMARY KEY (`mission_id`),
  KEY `fk_Mission_Strategy1_idx` (`strategy_id`),
  CONSTRAINT `fk_Mission_Strategy1` FOREIGN KEY (`strategy_id`) REFERENCES `strategy` (`strategy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mission_log
-- ----------------------------
DROP TABLE IF EXISTS `mission_log`;
CREATE TABLE `mission_log` (
  `missionlog_id` int(11) NOT NULL AUTO_INCREMENT,
  `mission_id` int(11) NOT NULL,
  `strategy_id` int(11) NOT NULL,
  `mission_name` mediumtext NOT NULL,
  `mission_remark` mediumtext,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`missionlog_id`),
  KEY `fk_Missionlog_Mission1_idx` (`mission_id`),
  KEY `fk_Missionlog_User1_idx` (`user_id`),
  CONSTRAINT `fk_Missionlog_Mission1` FOREIGN KEY (`mission_id`) REFERENCES `mission` (`mission_id`),
  CONSTRAINT `fk_Missionlog_User1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for plan
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `mission_id` int(11) NOT NULL,
  `plan_name` longtext NOT NULL,
  `plan_remark` longtext,
  PRIMARY KEY (`plan_id`),
  KEY `fk_Plan_Mission1_idx` (`mission_id`),
  CONSTRAINT `fk_Plan_Mission1` FOREIGN KEY (`mission_id`) REFERENCES `mission` (`mission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for plan_log
-- ----------------------------
DROP TABLE IF EXISTS `plan_log`;
CREATE TABLE `plan_log` (
  `planlog_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `mission_id` int(11) NOT NULL,
  `plan_name` longtext NOT NULL,
  `plan_remark` longtext,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`planlog_id`),
  KEY `fk_Plan_Plan1` (`plan_id`),
  KEY `fk_Planlog_User1` (`user_id`),
  CONSTRAINT `fk_Plan_Plan1` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`plan_id`),
  CONSTRAINT `fk_Planlog_User1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `role_remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for strategy
-- ----------------------------
DROP TABLE IF EXISTS `strategy`;
CREATE TABLE `strategy` (
  `strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `strategy_name` longtext NOT NULL,
  `strategy_year` varchar(4) NOT NULL,
  `strategy_remark` longtext,
  PRIMARY KEY (`strategy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for strategy_log
-- ----------------------------
DROP TABLE IF EXISTS `strategy_log`;
CREATE TABLE `strategy_log` (
  `strategylog_id` int(11) NOT NULL AUTO_INCREMENT,
  `strategy_id` int(11) NOT NULL,
  `strategy_name` longtext NOT NULL,
  `strategy_year` varchar(4) NOT NULL,
  `strategy_remark` longtext,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`strategylog_id`),
  KEY `fk_Strategy_log_Strategy_id1_idx` (`strategy_id`),
  KEY `fk_Strategy_log_User_id1_idx` (`user_id`),
  CONSTRAINT `fk_Strategy_log_Strategy_id1` FOREIGN KEY (`strategy_id`) REFERENCES `strategy` (`strategy_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Strategy_log_User_id1_idx` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for target
-- ----------------------------
DROP TABLE IF EXISTS `target`;
CREATE TABLE `target` (
  `target_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `target_name` varchar(255) NOT NULL,
  PRIMARY KEY (`target_id`),
  KEY `fk_target_plan1_idx` (`plan_id`),
  CONSTRAINT `fk_target_plan1` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`plan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for target_log
-- ----------------------------
DROP TABLE IF EXISTS `target_log`;
CREATE TABLE `target_log` (
  `targetlog_id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `target_name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`targetlog_id`),
  KEY `fk_Targetlog_target_idx` (`target_id`),
  KEY `fk_Strategy_log_User_id1_idx` (`user_id`),
  CONSTRAINT `fk_Targetlog_User1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `fk_Targetlog_target` FOREIGN KEY (`target_id`) REFERENCES `target` (`target_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for transaction
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `transac_id` int(11) NOT NULL AUTO_INCREMENT,
  `transactype_id` int(11) NOT NULL,
  `kpi_id` int(11) NOT NULL,
  `datatype_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `1_oct` double DEFAULT NULL,
  `2_nov` double DEFAULT NULL,
  `3_dec` double DEFAULT NULL,
  `4_jan` double DEFAULT NULL,
  `5_feb` double DEFAULT NULL,
  `6_mar` double DEFAULT NULL,
  `7_apr` double DEFAULT NULL,
  `8_may` double DEFAULT NULL,
  `9_jun` double DEFAULT NULL,
  `10_jul` double DEFAULT NULL,
  `11_aug` double DEFAULT NULL,
  `12_sep` double DEFAULT NULL,
  `transac_update` longtext NOT NULL,
  `transac_remark` longtext,
  PRIMARY KEY (`transac_id`,`transactype_id`,`kpi_id`,`datatype_id`),
  KEY `fk_Transaction_Transaction_type1_idx` (`transactype_id`),
  KEY `fk_Transaction_Kpi1_idx` (`kpi_id`),
  KEY `fk_Transaction_User1_idx` (`user_id`),
  KEY `fk_Transaction_Data_type1_idx` (`datatype_id`),
  CONSTRAINT `fk_Transaction_Data_type1` FOREIGN KEY (`datatype_id`) REFERENCES `data_type` (`datatype_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transaction_Kpi1` FOREIGN KEY (`kpi_id`) REFERENCES `kpi` (`kpi_id`),
  CONSTRAINT `fk_Transaction_Transaction_type1` FOREIGN KEY (`transactype_id`) REFERENCES `transaction_type` (`transactype_id`),
  CONSTRAINT `fk_Transaction_User1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for transaction_log
-- ----------------------------
DROP TABLE IF EXISTS `transaction_log`;
CREATE TABLE `transaction_log` (
  `transaclog_id` int(11) NOT NULL AUTO_INCREMENT,
  `transac_id` int(11) NOT NULL,
  `transactype_id` int(11) NOT NULL,
  `kpi_id` int(11) NOT NULL,
  `datatype_id` int(11) NOT NULL,
  `1_oct` double DEFAULT NULL,
  `2_nov` double DEFAULT NULL,
  `3_dec` double DEFAULT NULL,
  `4_jan` double DEFAULT NULL,
  `5_feb` double DEFAULT NULL,
  `6_mar` double DEFAULT NULL,
  `7_apr` double DEFAULT NULL,
  `8_may` double DEFAULT NULL,
  `9_jun` double DEFAULT NULL,
  `10_jul` double DEFAULT NULL,
  `11_aug` double DEFAULT NULL,
  `12_sep` double DEFAULT NULL,
  `transac_update` longtext NOT NULL,
  `transac_remark` longtext,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`transaclog_id`),
  KEY `fk_Transaction_log_Transaction1_idx` (`transac_id`,`transactype_id`,`kpi_id`,`datatype_id`),
  KEY `fk_Transaction_log_User_idx` (`user_id`),
  CONSTRAINT `fk_Transaction_log_Transaction1` FOREIGN KEY (`transac_id`, `transactype_id`, `kpi_id`, `datatype_id`) REFERENCES `transaction` (`transac_id`, `transactype_id`, `kpi_id`, `datatype_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Transaction_log_User` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for transaction_type
-- ----------------------------
DROP TABLE IF EXISTS `transaction_type`;
CREATE TABLE `transaction_type` (
  `transactype_id` int(11) NOT NULL AUTO_INCREMENT,
  `transactype_name` varchar(255) NOT NULL,
  PRIMARY KEY (`transactype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_contact` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_User_Role_idx` (`role_id`),
  KEY `fk_User_Department1_idx` (`dept_id`),
  CONSTRAINT `fk_User_Department1` FOREIGN KEY (`dept_id`) REFERENCES `department` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_User_Role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
