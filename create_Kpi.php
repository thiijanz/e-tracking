
<link rel="stylesheet" href="css/fstdropdown.css">
<script src="js/fstdropdown.js"></script>
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>

<?php 
include("config/config.php");
$user_id = $_SESSION['user_id'];
?>

<script type="text/javascript">
$(document).ready(function(){
            $(document).ready(function() {
                $('#target_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {target_name: $(this).val()},
                        url: 'ajax/showStrategy_year.php',
                        success: function(data) {
                          $('#strategy_year').html(data);
                        }
                    });
                    return false;
                });
                $('#target_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {target_name: $(this).val()},
                        url: 'ajax/showStrategy_name.php',
                        success: function(data) {
                           $('#strategy_name').html(data);
                        }
                    });
                    return false;
                });
                $('#target_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {target_name: $(this).val()},
                        url: 'ajax/showMission.php',
                        success: function(data) {
                           $('#mission_name').html(data);
                        }
                    });
                    return false;
                });
                $('#target_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {target_name: $(this).val()},
                        url: 'ajax/showPlan.php',
                        success: function(data) {
                           $('#plan_name').html(data);
                        }
                    });
                    return false;
                });
                $('#dept_group_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {dept_group_name: $(this).val()},
                        url: 'ajax/showDepartment.php',
                        success: function(data) {
                           $('#dept_name').html(data);
                        }
                    });
                    return false;
                });
            });
});

</script>
<script  type="text/javascript">
var scntDiv = $('#p_scents');
var i = $('#p_scents tr').size() + 1;

$('#addScnt').click(function() {
    scntDiv.append('<tr><td><input type="text" name="level" id="level"/></td><td><input type="text" name="score" id="score"/></td><td><a href="#" id="remScnt">ลบ</a></td></tr>');   
    i++;
    return false;
});

//Remove button
$(document).on('click', '#remScnt', function() {
    if (i > 2) {
        $(this).closest('tr').remove();
        i--;
    }
    return false;
});
</script>
<center>
<div class="blank">
<h3 style="color: #68AE00;">สร้างตัวชี้วัด</h3><br>
        <div class="portlet-grid panel-group">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a>ข้อมูลตัวชี้วัด</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
<form name="createKpi" action="config/addKpi.php" method="post">
<table class="table"> 
      <tr>
      <input type="hidden" name="user_id" value="<?php echo $user_id ;?>">
      <td width="181px"> ชื่อเป้าหมาย </td>
        <td> 
        <div style="width: 41.813em;">
        <select name="target_name" id="target_name" class="form-control fstdropdown-select" data-live-search="true" required oninvalid="this.setCustomValidity('กรุณาเลือกชื่อเป้าหมาย')" oninput="setCustomValidity('')" style="width: 580px" />
            <option value=""  selected disabled="disabled">เลือกชื่อเป้าหมาย</option>
           <?php
            $strSQL_mission = "SELECT * FROM target order by target_id";
            $objQuery_mission = mysql_query($strSQL_mission) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_mission))
              {
           ?>
              <option value="<?php echo $objResult["target_id"];?>"><?php echo $objResult["target_name"];?></option>
              <?php } ?>
        </select>
        </div>
        </td>
      </tr>
      <tr>
        <td width="181px"> ปีงบประมาณ </td>
        <td> 
        <select name="strategy_year" id="strategy_year" class="form-control" style=" width: 180px" disabled/>
        <option value="" >เลือกปีงบประมาณ</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อยุทธศาสตร์ </td>
        <td> 
        <select name="strategy_name" id="strategy_name" class="form-control"  disabled>
            <option value="">เลือกชื่อยุทธศาสตร์</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อกลยุทธ์</td>
        <td> 
        <select name="mission_name" id="mission_name" class="form-control"  disabled>
            <option value="">เลือกชื่อกลยุทธ์</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อโครงการ/แผนงาน</td>
        <td> 
        <select name="plan_name" id="plan_name" class="form-control"  readonly>
            <option value="">เลือกชื่อโครงการ/แผนงาน</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> กลุ่มหน่วยงาน</td>
        <td> 
        <select name="dept_group_name" id="dept_group_name" class="form-control" data-live-search="true" required oninvalid="this.setCustomValidity('กรุณาเลือกกลุ่มหน่วยงาน')" oninput="setCustomValidity('')">
            <option value=""  selected disabled="disabled">เลือกชื่อกลุ่มหน่วยงาน</option>
           <?php
            $strSQL_department_group = "SELECT * FROM department_group order by dept_group_id";
            $objQuery_department_group = mysql_query($strSQL_department_group) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_department_group))
              {
           ?>
              <option value="<?php echo $objResult["dept_group_id"];?>"><?php echo $objResult["dept_group_name"];?></option>
              <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> หน่วยงาน</td>
        <td> 
        <select name="dept_name" id="dept_name" class="form-control" data-live-search="true" required oninvalid="this.setCustomValidity('กรุณาเลือกหน่วยงาน')" oninput="setCustomValidity('')" disabled="disabled"/>
            <option value="" selected disabled="disabled" >เลือกชื่อหน่วยงาน</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อตัวชี้วัด</td>
        <td> 
        <input type="text" name="kpi_name" class="form-control" placeholder="ชื่อตัวชี้วัด" required oninvalid="this.setCustomValidity('กรุณากรอกชื่อตัวชี้วัด')" oninput="setCustomValidity('')" />
        </td>
      </tr>
      <tr>
      <td width="181px"> เป้าประสงค์ </td>
        <td> 
        <input type="text" name="objective" id="objective" placeholder="เป้าประสงค์" class="form-control" >
        </td>
      </tr>
      <tr>
      <td width="181px"> คำอธิบาย </td>
        <td> 
        <input type="text" name="description" id="description" placeholder="คำอธิบาย" class="form-control" >
        </td>
      </tr>
      <tr>
      <td width="181px"> เกณฑ์การให้คะแนน </td>
      <td>
     
      <table class="table table-bordered">
      <tr><td  align="center"> ระดับ</td>
          <td  align="center"> เกณฑ์การให้คะแนน</td></tr>
      <tbody id="p_scents">
        <tr><td align="center"><input type="hidden" name="level1" id="level1" value="1"/>1</td><td><input type="text" name="score1" id="score1" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level2" id="level2" value="2"/>2</td><td><input type="text" name="score2" id="score2" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level3" id="level3" value="3"/>3</td><td><input type="text" name="score3" id="score3" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level4" id="level4" value="4"/>4</td><td><input type="text" name="score4" id="score4" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level5" id="level5" value="5"/>5</td><td><input type="text" name="score5" id="score5" class="form-control" /></td></tr>
    </tbody>
      </table>
      </td>
      </tr>
      <tr>
      <td width="181px"> ได้รับจัดสรร </td>
        <td> 
        <input type="text" name="kpi_budgetask" id="kpi_budgetask" placeholder="ได้รับจัดสรร" class="form-control" >
        </td>
      </tr>
      <tr>
      <td width="181px"> การใช้จ่ายงบประมาณ </td>
        <td> 
        <input type="text" name="kpi_budgetgot" id="kpi_budgetgot" placeholder="การใช้จ่ายงบประมาณ" class="form-control">
        </td>
      </tr>
      <tr>
      <td width="181px"> ผลการดำเนินงาน </td>
        <td> 
        <textarea class="form-control" name="kpi_result" id="kpi_result" placeholder="ผลการดำเนินงาน" rows="3"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ค่าคะแนนที่ได้ </td>
        <td> 
        <textarea class="form-control" name="kpi_comment" id="kpi_comment" placeholder="ค่าคะแนนที่ได้" rows="3"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> คำชี้แจงการปฏิบัติงาน/มาตรการที่ได้ดำเนินการ </td>
        <td> 
        <textarea class="form-control" name="explanation" id="explanation" placeholder="คำชี้แจงการปฏิบัติงาน/มาตรการที่ได้ดำเนินการ" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ปัจจัยสนับสนุนต่อการดำเนินงาน </td>
        <td> 
        <textarea class="form-control" name="factor" id="factor" placeholder="ปัจจัยสนับสนุนต่อการดำเนินงาน" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> อุปสรรคต่อการดำเนินงาน </td>
        <td> 
        <textarea class="form-control" name="barrier" id="barrier" placeholder="อุปสรรคต่อการดำเนินงาน" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ข้อเสนอแนะสำหรับการดำเนินงานในปีต่อไป </td>
        <td> 
        <textarea class="form-control" name="suggestion" id="suggestion" placeholder="ข้อเสนอแนะสำหรับการดำเนินงานในปีต่อไป" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> หลักฐานอ้างอิง </td>
        <td> 
        <textarea class="form-control" name="reference" id="reference" placeholder="หลักฐานอ้างอิง" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ระยะเวลาดำเนินการ </td>
        <td> 
        <input type="text" class="form-control" name="time" placeholder="ระยะเวลาดำเนินการ" id="time" >
        </td>
      </tr>
      <tr>
      <td width="181px"> หมายเหตุ </td>
        <td> 
        <textarea class="form-control" rows="3" name="kpi_remark" placeholder="หมายเหตุ"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="2"> 
        <center><input type="reset" class="btn btn-danger" value="ยกเลิก"> &nbsp; <input type="submit" class="btn btn-success" value="ตกลง">
        </center>
        </td>
      </tr>
</table>
</form>
        </div>
      </div>
    </div> 
</div>
</center>
<script>
        $(document).ready(function () {
        $('#dept_group_name').on('selector change', function () {
                if ($(this).val() != '') {
                    $('#dept_name').prop('disabled', false);
                }
                else {
                    $('#dept_name').prop('disabled', true);
                }
            });
          
        });
    </script>