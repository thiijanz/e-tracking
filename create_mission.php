<link rel="stylesheet" href="css/bootstrap-select.css">
<script src="js/bootstrap-select.js"></script>
<?php 
include("config/config.php");
?>

<script type="text/javascript">
            $(document).ready(function() {
                $('#strategy_year').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {strategy_year: $(this).val()},
                        url: 'ajax/showStrategy.php',
                        success: function(data) {
                          $('#strategy_name').html(data).selectpicker('refresh'); //กรณีรีเฟรชข้อมูล ให้ทำงาน selectpicker ได้
                        }
                    });
                    return false;
                });
            });

</script>

<center>
<div class="blank">
<h3 style="color: #68AE00;">สร้างกลยุทธ์</h3><br>
        <div class="portlet-grid panel-group">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a>ข้อมูลกลยุทธ์</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
<form name="createAtrategy" action="config/addMission.php" method="post">
<table class="table"> 
      <tr>
        <td width="181px"> ปีงบประมาณ </td>
        <td> 
        <select name="strategy_year" id="strategy_year"  class="form-control" style="width: 180px" required oninvalid="this.setCustomValidity('กรุณาเลือกปีงบประมาณ')"  oninput="setCustomValidity('')" />
        <option value="" selected disabled="disabled">เลือกปีงบประมาณ</option>
        <?php
            $strSQL_year = "SELECT DISTINCT(`strategy_year`) FROM strategy order by strategy_year";
            $objQuery_year = mysql_query($strSQL_year) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_year))
              {
           ?>
            <option value="<?php echo $objResult["strategy_year"];?>"><?php echo $objResult["strategy_year"];?></option>
            
        <?php } ?>    
        </select>
     
        </td>
      </tr>
      <tr>
  
      <td width="181px"> ชื่อยุทธศาสตร์ </td>
        <td> 
        <select name="strategy_name" id="strategy_name" class="selectpicker form-control"  data-live-search="true" data-width="650px" />
            <option value=""  selected disabled="disabled">เลือกชื่อยุทธศาสตร์</option>
           
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อกลยุทธ์ </td>
        <td> 
        <input type="text" name="mission_name" class="form-control" placeholder="ชื่อกลยุทธ์" required oninvalid="this.setCustomValidity('กรุณากรอกชื่อกลยุทธ์')" oninput="setCustomValidity('')" />
        </td>
      </tr>
      <tr>
      <td width="181px"> หมายเหตุ </td>
        <td> 
        <textarea class="form-control" rows="3" name="mission_remark"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="2"> 
        <center><input type="reset" class="btn btn-danger" value="ยกเลิก"> &nbsp; <input type="submit" class="btn btn-success" value="ตกลง"></center>
        </td>
      </tr>
</table>
</form>
        </div>
      </div>
    </div> 
</div>
</center>
