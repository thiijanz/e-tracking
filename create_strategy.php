
<center>
<div class="blank">
<h3 style="color: #68AE00;">สร้างยุทธศาสตร์</h3><br>
        <div class="portlet-grid panel-group">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a>ข้อมูลยุทธศาสตร์</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
<form name="createStrategy" method="post"  action="config/addStrategy.php" >
<!-- onsubmit="return validate()" -->
<table class="table"> 
      <tr>
        <td> ปีงบประมาณ </td>
        <td> 
				<div>
        <input type="number" name="strategy_year" id="strategy_year" class="form-control" placeholder="ปีงบประมาณ" required oninvalid="this.setCustomValidity('กรุณากรอกปีงบประมาณ')" oninput="setCustomValidity('')" style=" width: 180px" />
				</div>
				</td>
      </tr>
      <tr>
      <td> ชื่อยุทธศาสตร์ </td>
        <td> 
				<div>
        <input type="text" name="strategy_name" id="strategy_name" class="form-control" placeholder="ชื่อยุทธศาสตร์" required oninvalid="this.setCustomValidity('กรุณากรอกชื่อยุทธศาสตร์')" oninput="setCustomValidity('')"  />
        </div>
				</td>
      </tr>
      <tr>
      <td> หมายเหตุ </td>
        <td> 
        <textarea class="form-control" name="strategy_remark" rows="3"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="2"> 
        <center><input type="reset" class="btn btn-danger" value="ยกเลิก"> &nbsp; <input type="submit" class="btn btn-success"  value="ตกลง" ></center>
        </td>
      </tr>
</table>
</form>
        </div>
      </div>
    </div> 
</div>

</center>
