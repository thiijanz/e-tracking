<link rel="stylesheet" href="css/bootstrap-select.css">
<script src="js/bootstrap-select.js"></script>

<?php 
include("config/config.php");
?>

<script type="text/javascript">
$(document).ready(function(){
            $(document).ready(function() {
                $('#plan_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {plan_name: $(this).val()},
                        url: 'ajax/showStrategy_year.php',
                        success: function(data) {
                          $('#strategy_year').html(data);
                        }
                    });
                    return false;
                });
                $('#plan_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {plan_name: $(this).val()},
                        url: 'ajax/showStrategy_name.php',
                        success: function(data) {
                           $('#strategy_name').html(data);
                        }
                    });
                    return false;
                });
                $('#plan_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {plan_name: $(this).val()},
                        url: 'ajax/showMission.php',
                        success: function(data) {
                           $('#mission_name').html(data);
                        }
                    });
                    return false;
                });
            });
});

</script>
<center>
<div class="blank">
    	<h3 style="color: #68AE00;">สร้างเป้าหมาย</h3><br>
        <div class="portlet-grid panel-group">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a>ข้อมูลเป้าหมาย</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
<form name="createTarget" action="config/addTarget.php" method="post">
<table class="table"> 
      <tr>
      <td width="181px"> ชื่อโครงการ/แผนงาน </td>
        <td> 
     
        <select name="plan_name" id="plan_name" class="form-control selectpicker" data-live-search="true" required oninvalid="this.setCustomValidity('กรุณาเลือกชื่อโครงการ/แผนงาน')" oninput="setCustomValidity('')" style="width: 580px" />
            <option value=""  selected disabled="disabled">เลือกชื่อโครงการ/แผนงาน</option>
           <?php
            $strSQL_mission = "SELECT * FROM plan order by plan_id";
            $objQuery_mission = mysql_query($strSQL_mission) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_mission))
              {
           ?>
              <option value="<?php echo $objResult["plan_id"];?>"><?php echo $objResult["plan_name"];?></option>
              <?php } ?>
        </select>
        </td>
      </tr>
      <tr>
        <td width="181px"> ปีงบประมาณ </td>
        <td> 
        <select name="strategy_year" id="strategy_year" class="form-control" style=" width: 180px" disabled/>
        <option value="" >เลือกปีงบประมาณ</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อยุทธศาสตร์ </td>
        <td> 
        <select name="strategy_name" id="strategy_name" class="form-control"  disabled>
            <option value="">เลือกชื่อยุทธศาสตร์</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อกลยุทธ์</td>
        <td> 
        <select name="mission_name" id="mission_name" class="form-control"  disabled>
            <option value="">เลือกชื่อกลยุทธ์</option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อเป้าหมาย</td>
        <td> 
        <input type="text" class="form-control" name="target_name" placeholder="ชื่อเป้าหมาย" required oninvalid="this.setCustomValidity('กรุณากรอกชื่อเป้าหมาย')" oninput="setCustomValidity('')"/>
        </td>
      </tr>
      <tr>
        <td colspan="2"> 
        <center><input type="reset" class="btn btn-danger" value="ยกเลิก"> &nbsp; <input type="submit" class="btn btn-success" value="ตกลง">
        </center>
        </td>
      </tr>
</table>
</form>
        </div>
      </div>
    </div> 
</div>
</center>