<style type="text/css">
.table-responsive {
    width: 100%;
    margin-bottom: 15px;
    overflow-y: hidden;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #ddd;
  }
  .table-responsive > .table {
    margin-bottom: 0;
  }
  .table-responsive > .table > thead > tr > th,
  .table-responsive > .table > tbody > tr > th,
  .table-responsive > .table > tfoot > tr > th,
  .table-responsive > .table > thead > tr > td,
  .table-responsive > .table > tbody > tr > td,
  .table-responsive > .table > tfoot > tr > td {
    white-space: nowrap;
  }
  .table-responsive > .table-bordered {
    border: 0;
  }
  .table-responsive > .table-bordered > thead > tr > th:first-child,
  .table-responsive > .table-bordered > tbody > tr > th:first-child,
  .table-responsive > .table-bordered > tfoot > tr > th:first-child,
  .table-responsive > .table-bordered > thead > tr > td:first-child,
  .table-responsive > .table-bordered > tbody > tr > td:first-child,
  .table-responsive > .table-bordered > tfoot > tr > td:first-child {
    border-left: 0;
  }
  .table-responsive > .table-bordered > thead > tr > th:last-child,
  .table-responsive > .table-bordered > tbody > tr > th:last-child,
  .table-responsive > .table-bordered > tfoot > tr > th:last-child,
  .table-responsive > .table-bordered > thead > tr > td:last-child,
  .table-responsive > .table-bordered > tbody > tr > td:last-child,
  .table-responsive > .table-bordered > tfoot > tr > td:last-child {
    border-right: 0;
  }
  .table-responsive > .table-bordered > tbody > tr:last-child > th,
  .table-responsive > .table-bordered > tfoot > tr:last-child > th,
  .table-responsive > .table-bordered > tbody > tr:last-child > td,
  .table-responsive > .table-bordered > tfoot > tr:last-child > td {
    border-bottom: 0;
  }
</style>

<?php 
include("config/config.php");
error_reporting(0);
if(isset($_GET['mission_id'])) {
$mission_id = $_GET['mission_id'];
$strSQL = "SELECT missionlog_id, mission_id, mission_log.strategy_id, strategy_name, strategy_year, mission_name, mission_remark,DATE_FORMAT(date,'%e/%m/%Y (%T)') as date, user_name from mission_log join user
on mission_log.user_id = user.user_id join strategy on strategy.strategy_id = mission_log.strategy_id where mission_id= '$mission_id' ";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 5;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order by date desc LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>

<div class="container">
<h3 style="color: #68AE00;" align="center">ประวัติการแก้ไข</h3> <br>
<div class="table-responsive">
<table class="table table-bordered">
      <tr align="center" class="success">
        <td>#</td>
        <td>ปีงบประมาณ</td>
        <td>ชื่อยุทธศาสตร์</td>
        <td>ชื่อกลยุทธ์</td>
        <td>หมายเหตุ</td>
        <td>แก้ไขเมื่อ</td>
        <td>ผู้แก้ไข</td>
      </tr>
    
    <tbody>
   
    <?php 
       while($resultHis = mysql_fetch_array($objQuery)){
      ?>
      <tr>
      <td><?= (($Per_Page*($Page-1))+$i) ?>.</td>
      <td align="center"><?php echo $resultHis["strategy_year"]?></td>
      <td><?php echo $resultHis["strategy_name"]?></td>
      <td><?php echo $resultHis["mission_name"]?></td>
      <td><?php echo $resultHis["mission_remark"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["date"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["user_name"]?></td>
      </tr>
      <?php  $i++; }  ?>
    </tbody>
  </table>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<center>ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$mission_id&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$mission_id&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$mission_id&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}?>
</div>


<?php }
else if (isset($_GET['strategy_id'])) {
$strategy_id = $_GET['strategy_id'];
$strSQL = "SELECT strategylog_id, strategy_id, strategy_name, strategy_year, strategy_remark,DATE_FORMAT(date,'%e/%m/%Y (%T)') as date, user_name from strategy_log join user
on  strategy_log.user_id = user.user_id where strategy_id = '$strategy_id' ";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 5;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order by date desc LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>

<div class="container">
<h3 style="color: #68AE00;" align="center">ประวัติการแก้ไข</h3> <br>
<div class="table-responsive">
<table class="table table-bordered">
      <tr align="center" class="success">
        <td>#</td>
        <td>ปีงบประมาณ</td>
        <td>ชื่อยุทธศาสตร์</td>
        <td>หมายเหตุ</td>
        <td>แก้ไขเมื่อ</td>
        <td>ผู้แก้ไข</td>
      </tr>
    <tbody>
    <?php 
       while($resultHis = mysql_fetch_array($objQuery)){
      ?>
      <tr>
      <td><?= (($Per_Page*($Page-1))+$i) ?>.</td>
      <td align="center"><?php echo $resultHis["strategy_year"]?></td>
      <td><?php echo $resultHis["strategy_name"]?></td>
      <td><?php echo $resultHis["strategy_remark"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["date"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["user_name"]?></td>
      </tr>
      <?php  $i++; }  ?>
    </tbody>
  </table>
</div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<center>ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$strategy_id&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$strategy_id&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$strategy_id&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}?>
       
<?php }
else if (isset($_GET['plan_id'])) {
$plan_id = $_GET['plan_id'];
$strSQL = "SELECT planlog_id, plan_id, mission_name, plan_name, plan_remark, DATE_FORMAT(date,'%e/%m/%Y (%T)') as date, user_name from plan_log join user
on  plan_log.user_id = user.user_id join mission on plan_log.mission_id = mission.mission_id where plan_id = '$plan_id' ";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 5;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order by date desc LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>
<div class="container">
<h3 style="color: #68AE00;" align="center">ประวัติการแก้ไข</h3> <br>
<div class="table-responsive">
<table class="table table-bordered">
      <tr align="center" class="success">
        <td>#</td>
        <td>ชื่อกลยุทธ์</td>
        <td>ชื่อโครงการ/แผนงาน</td>
        <td>หมายเหตุ</td>
        <td>แก้ไขเมื่อ</td>
        <td>ผู้แก้ไข</td>
      </tr>
    <tbody>
    <?php 
       while($resultHis = mysql_fetch_array($objQuery)){
      ?>
      <tr>
      <td><?= (($Per_Page*($Page-1))+$i) ?>.</td>
      <td><?php echo $resultHis["mission_name"]?></td>
      <td><?php echo $resultHis["plan_name"]?></td>
      <td><?php echo $resultHis["plan_remark"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["date"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["user_name"]?></td>
      </tr>
      <?php  $i++; }  ?>
    </tbody>
  </table>
</div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<center>ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$plan_id&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$plan_id&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$plan_id&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}?>

<?php }
else if (isset($_GET['target_id'])) {
$target_id = $_GET['target_id'];
$strSQL = "SELECT targetlog_id, target_id, plan_name, target_name, DATE_FORMAT(date,'%e/%m/%Y (%T)') as date, user_name from target_log join user
on  target_log.user_id = user.user_id join plan on target_log.plan_id = plan.plan_id where target_id = '$target_id' ";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 5;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order by date desc LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>
<div class="container">
<h3 style="color: #68AE00;" align="center">ประวัติการแก้ไข</h3> <br>
<div class="table-responsive">
<table class="table table-bordered">
      <tr align="center" class="success">
        <td>#</td>
        <td>ชื่อโครงการ/แผนงาน</td>
        <td>ชื่อเป้าหมาย</td>
        <td>แก้ไขเมื่อ</td>
        <td>ผู้แก้ไข</td>
      </tr>
    <tbody>
    <?php 
       while($resultHis = mysql_fetch_array($objQuery)){
      ?>
      <tr>
      <td><?= (($Per_Page*($Page-1))+$i) ?>.</td>
      <td><?php echo $resultHis["plan_name"]?></td>
      <td><?php echo $resultHis["target_name"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["date"]?></td>
      <td align="center" width="180px"><?php echo $resultHis["user_name"]?></td>
      </tr>
      <?php  $i++; }  ?>
    </tbody>
  </table>
</div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<center>ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$target_id&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$target_id&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$target_id&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}

?>

<?php }
else if (isset($_GET['kpi_id'])) {
$kpi_id = $_GET['kpi_id'];
$strSQL = "SELECT kl.kpi_id, kl.kpi_name, kl.kpi_remark, kl.kpi_budgetask, kl.kpi_budgetgot, kl.kpi_count, kl.kpi_result, kl.kpi_comment, t.target_id, 
t.target_name , p.plan_id, p.plan_name, p.plan_remark, m.mission_id, m.strategy_id, s.strategy_year, s.strategy_name, s.strategy_remark, 
m.mission_name, m.mission_remark, d.dept_name, dg.dept_group_name, DATE_FORMAT(date,'%e/%m/%Y (%T)') as date, u.user_name FROM kpi_log kl 
join kpi k on kl.kpi_id = k.kpi_id join target t on k.target_id = t.target_id join plan p on t.plan_id = p.plan_id 
join mission m on p.mission_id = m.mission_id  join strategy s on m.strategy_id = s.strategy_id join department d on k.dept_id = d.dept_id
 join department_group dg on d.dept_group_id = dg.dept_group_id join user u on kl.user_id = u.user_id where kl.kpi_id = '$kpi_id' ";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 5;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order by date desc LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);

?>
<div class="container">
<h3 style="color: #68AE00;" align="center">ประวัติการแก้ไข</h3> <br>
<div class="table-responsive">
<table class="table table-bordered">
    <tr align="center" class="success" >
        <td>#</td>
        <td>ชื่อโครงการ/แผนงาน</td>
        <td>ชื่อเป้าหมาย</td>
        <td>กลุ่มหน่วยงาน</td>
        <td>หน่วยงาน</td>
        <td>ชื่อตัวชี้วัด</td>
        <td>ได้รับจัดสรร</td>
        <td>การใช้จ่ายงบประมาณ</td>
        <td>ผลการดำเนินงาน</td>
        <td>ผลการประเมิน</td>
        <td>หมายเหตุ</td>
        <td>หมายเหตุ</td>
        <td>ผู้แก้ไข</td>
      </tr>
    <?php
      while($objResult = mysql_fetch_array($objQuery))
      {
    ?>
      <tr>
      <td><?= (($Per_Page*($Page-1))+$i) ?>.</td>
      <td><?php echo $objResult["plan_name"]?></td>
      <td><?php echo $objResult["target_name"]?></td>
      <td><?php echo $objResult["dept_group_name"]?></td>
      <td><?php echo $objResult["dept_name"]?></td>
      <td><?php echo $objResult["kpi_name"]?></td>
      <td><?php echo $objResult["kpi_budgetask"]?></td>
      <td><?php echo $objResult["kpi_budgetgot"]?></td>
      <td><?php echo $objResult["kpi_result"]?></td>
      <td><?php echo $objResult["kpi_comment"]?></td>
      <td><?php echo $objResult["kpi_remark"]?></td>
      <td align="center"><?php echo $objResult["date"]?></td>
      <td align="center"><?php echo $objResult["user_name"]?></td>
      </tr>
      <?php  $i++; }  ?>
  </table>
  </div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<center>ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$kpi_id&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$kpi_id&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=history_log&kpi_id=$kpi_id&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}

?>
<?php } ?>
