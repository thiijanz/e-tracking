<?php
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<title>ระบบรายงานผลการดำเนินงานของตัวชี้วัดตามคำรับรองปฏิบัติราชการ</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				<form class="login-form" action="config/check_login.php" method="post">
					<span class="login100-form-title p-b-55">
						<center><img src="images/logo_doeb.png" width="255px" height="70px" style="width:100%;"></center>
					</span>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="user_login" placeholder="ชื่อผู้ใช้" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-envelope"></span>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="user_pass" placeholder="รหัสผ่าน" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
					</div>

					
					<div class="container-login100-form-btn p-t-25">
						<button class="login100-form-btn">
							เข้าสู่ระบบ
						</button>
					</div>

					
				</form>
			</div>
		</div>
	</div>
	


</body>
</html>