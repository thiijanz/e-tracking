<?php
if(empty($_POST['select_year'])){
  $select_year = date('Y')+543;
}else{$select_year = $_POST['select_year'];}
?>



<link rel="stylesheet" href="css/fstdropdown.css">
<script src="js/fstdropdown.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style type="text/css">
	.modal-confirm {		
		color: #636363;
		width: 400px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
        text-align: center;
		font-size: 14px;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -10px;
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -2px;
	}
	.modal-confirm .modal-body {
		color: #999;
	}
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;		
		border-radius: 5px;
		font-size: 13px;
		padding: 10px 15px 25px;
	}
	.modal-confirm .modal-footer a {
		color: #999;
	}		
	.modal-confirm .icon-box {
		width: 80px;
		height: 80px;
		margin: 0 auto;
		border-radius: 50%;
		z-index: 9;
		text-align: center;
		border: 3px solid ;
	}
	.modal-confirm .icon-box i {
		/* color: #f15e5e; */
		font-size: 46px;
		display: inline-block;
		margin-top: 13px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #60c7c1;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
		min-width: 120px;
        border: none;
		min-height: 40px;
		border-radius: 3px;
		margin: 0 5px;
		outline: none !important;
    }
	.modal-confirm .btn-info {
        background: #c1c1c1;
    }
    .modal-confirm .btn-info:hover, .modal-confirm .btn-info:focus {
        background: #a8a8a8;
    }
    .modal-confirm .btn-danger {
        background: #f15e5e;
    }
    .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
        background: #ee3535;
    }
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
</style>
<script>
$(document).ready(function() {
  $("#searchplan").on("keypress click input", function() {
    var val = $(this).val();

    if (val.length) {
      $(".portlet-grid .panel-success").hide().filter(function() {
        return $('.panel-heading .panel-title', this).text().toLowerCase().indexOf(val.toLowerCase()) > -1;
      }).show();
    } else {
      $(".portlet-grid .panel-success").show();
    }
  });
});
</script>
<script>
$(document).ready(function() {
		$('#myModal').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var id = button.data('id');
			var name = button.data('name');

			var modal = $(this);
			modal.find('#btnYes').val(id);
			modal.find('#name').text(name);

		});

$(document).on("click", "#btnYes", function () 
{
	$.ajax({
    url:"ajax/deleteRecords.php",
    type:"POST",
	  data: {kpi_id: $(this).val()},
    success: function(data)
	{
		$('#myModal1').modal("show");
		$('#success1').text(data);
    	fetch_data(); 

    },
     error: function(){
       alert("error");
     }  
   });  

    $("#listKpi").load(location.href+" #listKpi>*","");
  	$('#myModal').modal('hide');
	 
 });
});
</script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#target_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {target_id: $(this).val()},
                        url: 'ajax/showDataModal.php',
                        dataType:"json",
                        success: function(data) {
                          $('#strategy_year').val(data.strategy_year);
                          $('#strategy_name').val(data.strategy_id);
                          $('#plan_name').val(data.plan_name);
                          $('#plan_name2').val(data.plan_id);
                          $('#mission_name').val(data.mission_name); 
													$('#editModal').modal('show');  
                        }
                    });
                    return false;
                });
            });

</script>

<script type="text/javascript">
            $(document).ready(function() {
                $('#dept_group_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {dept_group_name: $(this).val()},
                        url: 'ajax/showDepartment.php',
                        success: function(data) {
                          $('#dept_name').html(data);
                          $('#dept_name2').html(data); 
                          $('#editModal').modal('show');  
                        }
                    });
                    return false;
                });
            });

</script>
<script>
$(document).ready(function() {
	$(document).on('click', '.edit_data', function(){  
           var kpi_id = $(this).attr("id");  
           $.ajax({  
                url:"ajax/showDataModal.php",  
                method:"POST",  
                data:{kpi_id:kpi_id},  
                dataType:"json",  
                success:function(data){  
										 $('#title').text(data.kpi_name);
                     $('#kpi_id').val(data.kpi_id);  
                     $('#mission_name').val(data.mission_name); 
                     $('#strategy_name').val(data.strategy_id);
                     $('#plan_name').val(data.plan_name);
                     $('#plan_name2').val(data.plan_id);
                     $('#target_name').val(data.target_id); 
                     $('#dept_group_name').val(data.dept_group_id); 
                     $('#dept_name').val(data.dept_id); 
                     $('#dept_name2').val(data.dept_id);
                     $('#kpi_name').val(data.kpi_name);
                     $('#kpi_remark').val(data.kpi_remark);
                     $('#kpi_budgetask').val(data.kpi_budgetask); 
                     $('#kpi_budgetgot').val(data.kpi_budgetgot); 
                     $('#kpi_count').val(data.kpi_count); 
                     $('#kpi_result').val(data.kpi_result); 
                     $('#kpi_comment').val(data.kpi_comment);       
                     $('#plan_name1').val(data.plan_id);
                     $('#target_name1').val(data.target_id); 
                     $('#dept_name1').val(data.dept_id); 
                     $('#kpi_name1').val(data.kpi_name);
                     $('#kpi_remark1').val(data.kpi_remark);
                     $('#kpi_budgetask1').val(data.kpi_budgetask); 
                     $('#kpi_budgetgot1').val(data.kpi_budgetgot); 
                     $('#kpi_count1').val(data.kpi_count); 
                     $('#kpi_result1').val(data.kpi_result); 
                     $('#kpi_comment1').val(data.kpi_comment);  
                     $('#objective').val(data.objective);   
                     $('#description').val(data.description);  
                     $('#explanation').val(data.explanation);  
                     $('#factor').val(data.factor);  
                     $('#barrier').val(data.barrier);  
                     $('#suggestion').val(data.suggestion);  
                     $('#reference').val(data.reference);  
                     $('#time').val(data.time);  
                     $('#level1').val(data.level1);  
                     $('#level2').val(data.level2);  
                     $('#level3').val(data.level3);  
                     $('#level4').val(data.level4);  
                     $('#level5').val(data.level5);  
                     $('#score1').val(data.score1);  
                     $('#score2').val(data.score2);  
                     $('#score3').val(data.score3);  
                     $('#score4').val(data.score4);  
                     $('#score5').val(data.score5);
                     $('#objective1').val(data.objective);   
                     $('#description1').val(data.description);  
                     $('#explanation1').val(data.explanation);  
                     $('#factor1').val(data.factor);  
                     $('#barrier1').val(data.barrier);  
                     $('#suggestion1').val(data.suggestion);  
                     $('#reference1').val(data.reference);  
                     $('#time1').val(data.time);  
                     $('#llevel1').val(data.level1);  
                     $('#llevel2').val(data.level2);  
                     $('#llevel3').val(data.level3);  
                     $('#llevel4').val(data.level4);  
                     $('#llevel5').val(data.level5);  
                     $('#sscore1').val(data.score1);  
                     $('#sscore2').val(data.score2);  
                     $('#sscore3').val(data.score3);  
                     $('#sscore4').val(data.score4);  
                     $('#sscore5').val(data.score5);
                     $('#criterion_id').val(data.criterion_id);
                     $('#editModal').modal('show');  
                }  
           });  
      });
      $('#edit_form').on("submit", function(event){  
           event.preventDefault();  
                $.ajax({  
                     url:"ajax/edit_kpi.php",  
                     method:"POST",  
                     data:$('#edit_form').serialize(),  
                     beforeSend:function(){  
                          $('#edit').val("กำลังบันทึก..");  
                     },  
                     success:function(data){  
                          $('#edit_form')[0].reset();  
                          $('#editModal').modal('hide');
													$('#edit').val("บันทึก");
													$("#listKpi").load(location.href+" #listKpi>*","");  
													$('#myModal1').modal("show");
													$('#success1').text(data);
    											fetch_data();
                          
                     }  
                });  
      });   
	});
</script>
<?php
error_reporting(0);
include("config/config.php");
$user_id = $_SESSION['user_id'];
$strSQL = "SELECT k.kpi_id, k.kpi_name, k.kpi_remark, k.kpi_budgetask, k.kpi_budgetgot, k.kpi_count, k.kpi_result, k.kpi_comment, k.user_id, k.objective, k.description, k.explanation, k.factor, k.barrier, k.suggestion, k.reference, k.time, t.target_id, t.target_name , p.plan_id, p.plan_name, p.plan_remark, 
m.mission_id, m.strategy_id, s.strategy_year, s.strategy_name, s.strategy_remark, m.mission_name, m.mission_remark, d.dept_name, dg.dept_group_name , c.criterion_id, c.level1, c.level2, c.level3, c.level4, c.level5, c.score1, c.score2, c.score3, c.score4, c.score5 FROM kpi k join target t on k.target_id = t.target_id join plan p on t.plan_id = p.plan_id 
join mission m on p.mission_id = m.mission_id  join strategy s on m.strategy_id = s.strategy_id join department d on k.dept_id = d.dept_id join department_group dg on d.dept_group_id = dg.dept_group_id left join criterion c on c.kpi_id = k.kpi_id 
WHERE s.strategy_year ='".$select_year."'";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 100;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order  by k.kpi_id ASC LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>
<center>
<div class="blank">
<h3 style="color: #68AE00;">รายการตัวชี้วัด</h3><br>
<form action="main.php?menu=list_kpi" id="select_year" name="select_year" method="POST" >
<div style="color: #68AE00;">ปีงบประมาณ 
    
      <select id="select_year" name="select_year" onchange="this.form.submit()">
          <option value="0">เลือกปีงบประมาณ : </option>
<?php
$strSQLyear ="SELECT DISTINCT strategy_year from strategy ORDER by strategy_year DESC";
$objQueryyear  = mysql_query($strSQLyear);
while($objyearResult = mysql_fetch_array($objQueryyear))
{?>       <option value="<?php echo $objyearResult['strategy_year'] ; ?>"
          <?php if($select_year==$objyearResult['strategy_year']){echo "selected";} ?> >
          <?php echo $objyearResult['strategy_year'] ; ?> </option>
<?php } ?>
      </select>
</div>
</form>
<br>
<input type="text" name="searchplan" id="searchplan" placeholder="ค้นหาตัวชี้วัด.... " class="form-control" style="width: 450px;">
<br>
<form id="listKpi" method="post">

 <div class="portlet-grid panel-group">
 <?php
      while($objResult = mysql_fetch_array($objQuery))
      {
    ?>
    <div class="panel panel-success"  id="accordion">
 
      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $objResult["kpi_id"];?>" >
        <h4 class="panel-title">
          <a href="#"><?=  (($Per_Page*($Page-1))+$i) ?>. <?php echo $objResult["kpi_name"];?></a>
        </h4>
      </div>
      <div id="<?php echo $objResult["kpi_id"];?>" class="panel-collapse collapse">
        <div class="panel-body">

<table class="table"> 
    <tr>
      <td  width="181px"> ชื่อกลยุทธ์ </td>
        <td> 
        <input type="text" name="mission_name" value="<?php echo $objResult["mission_name"];?>" class="form-control" disabled>
        
        </td>
      </tr>
      <tr>
        <td width="181px"> ปีงบประมาณ </td>
        <td> 
        <select name="strategy_year" class="form-control"  disabled style="width: 180px">
            <option value="<?php echo $objResult["strategy_year"];?>"><?php echo $objResult["strategy_year"];?></option>
        </select>
        <!-- <input type="text" class="form-control"> -->
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อยุทธศาสตร์ </td>
        <td> 
        <input type="text" name="strategy_name" value="<?php echo $objResult["strategy_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อโครงการ/แผนงาน </td>
        <td> 
        <input type="text" name="plan_name" value="<?php echo $objResult["plan_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อเป้าหมาย </td>
        <td> 
        <input type="text" name="target_name" value="<?php echo $objResult["target_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> กลุ่มหน่วยงาน </td>
        <td> 
        <input type="text" name="dept_group_name" value="<?php echo $objResult["dept_group_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> หน่วยงาน </td>
        <td> 
        <input type="text" name="dept_name" value="<?php echo $objResult["dept_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อตัวชี้วัด </td>
        <td> 
        <input type="text" name="kpi_name" value="<?php echo $objResult["kpi_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> เป้าประสงค์ </td>
        <td> 
        <input type="text" name="objective" value="<?php echo $objResult["objective"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> คำอธิบาย </td>
        <td> 
        <input type="text" name="description" value="<?php echo $objResult["description"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> เกณฑ์การให้คะแนน </td>
      <td>
     
      <table class="table table-bordered">
      <tr><td  align="center"> ระดับ</td>
          <td  align="center"> เกณฑ์การให้คะแนน</td></tr>
      <tbody id="p_scents">
        <tr><td align="center"><input type="hidden" name="level1" value="<?php echo $objResult["level1"];?>"/>1</td><td><input type="text" name="score1" value="<?php echo $objResult["score1"];?>" class="form-control" readonly/></td></tr>
        <tr><td align="center"><input type="hidden" name="level2" value="<?php echo $objResult["level2"];?>"/>2</td><td><input type="text" name="score2" value="<?php echo $objResult["score2"];?>" class="form-control" readonly/></td></tr>
        <tr><td align="center"><input type="hidden" name="level3" value="<?php echo $objResult["level3"];?>"/>3</td><td><input type="text" name="score3" value="<?php echo $objResult["score3"];?>" class="form-control" readonly/></td></tr>
        <tr><td align="center"><input type="hidden" name="level4" value="<?php echo $objResult["level4"];?>"/>4</td><td><input type="text" name="score4" value="<?php echo $objResult["score4"];?>" class="form-control" readonly/></td></tr>
        <tr><td align="center"><input type="hidden" name="level5" value="<?php echo $objResult["level5"];?>"/>5</td><td><input type="text" name="score5" value="<?php echo $objResult["score5"];?>" class="form-control" readonly/></td></tr>
    </tbody>
      </table>
      </td>
      </tr>
      <tr>
      <td width="181px"> ได้รับจัดสรร </td>
        <td> 
        <input type="text" name="kpi_budgetask" value="<?php echo $objResult["kpi_budgetask"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> การใช้จ่ายงบประมาณ </td>
        <td> 
        <input type="text" name="kpi_budgetgot" value="<?php echo $objResult["kpi_budgetgot"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> ผลการดำเนินงาน </td>
        <td> 
        <input type="text" name="kpi_result" value="<?php echo $objResult["kpi_result"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> ค่าคะแนนที่ได้ </td>
        <td> 
        <input type="text" name="kpi_comment" value="<?php echo $objResult["kpi_comment"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> คำชี้แจงการปฏิบัติงาน/มาตรการที่ได้ดำเนินการ </td>
        <td> 
        <textarea class="form-control" name="explanation" value="<?php echo $objResult["explanation"];?>" rows="4" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ปัจจัยสนับสนุนต่อการดำเนินงาน </td>
        <td> 
        <textarea class="form-control" name="factor" value="<?php echo $objResult["factor"];?>" rows="4" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> อุปสรรคต่อการดำเนินงาน </td>
        <td> 
        <textarea class="form-control" name="barrier" value="<?php echo $objResult["barrier"];?>"  rows="4" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ข้อเสนอแนะสำหรับการดำเนินงานในปีต่อไป </td>
        <td> 
        <textarea class="form-control" name="suggestion" value="<?php echo $objResult["suggestion"];?>" rows="4" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> หลักฐานอ้างอิง </td>
        <td> 
        <textarea class="form-control" name="reference" value="<?php echo $objResult["reference"];?>" rows="4" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ระยะเวลาดำเนินการ </td>
        <td> 
        <input type="text" class="form-control" name="time" value="<?php echo $objResult["time"];?>" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> หมายเหตุ </td>
        <td> 
        <textarea class="form-control" name="kpi_remark" value="<?php echo $objResult["kpi_remark"];?>" rows="3" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td colspan="2"> 
        <center>
        <a href="#myModal" data-toggle="modal" data-name="<?php echo $objResult["kpi_name"];?>" data-id="<?php echo $objResult["kpi_id"];?>"> <button class="btn btn-danger"> ลบ </button></a>
        &nbsp; 
				<input type="button" name="edit" value="แก้ไข" id="<?php echo $objResult["kpi_id"];?>" class="btn btn-warning edit_data" />
        </center>
      </td>
      </tr>
      <tr>
      <td colspan="2">
      <?php 
        $history = mysql_query("SELECT distinct(kpi_log.kpi_id) FROM kpi_log join kpi on kpi_log.kpi_id = kpi.kpi_id ");
        // $Rows = mysql_num_rows($history);
        while($resultHis = mysql_fetch_array($history)){
            $hisid =  $resultHis["kpi_id"];
            $oldid = $objResult["kpi_id"];
            if ($oldid == $hisid) {
      ?>
      <a href="main.php?menu=history_log&kpi_id=<?php echo $objResult["kpi_id"];?>"  class="view_history"> <h5>ประวัติการแก้ไข</h5></a>
        <?php }
    } ?>
      </td>
      </tr>
</table>

        </div>
      </div>
    </div> 
    <?php $i++; } ?>
</div>
</form>
</div>
      
<br>
ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=list_kpi&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=list_kpi&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=list_kpi&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}

?>
</center> 
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box" style="color: #f15e5e;">
					<i class="material-icons">&#xE5CD;</i>
				</div>				
				<h4 class="modal-title">คุณแน่ใจไหม ?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>คุณต้องการลบ </p>
				<p style="color: red;" id="name" name="name"> </p>
				<p>ใช่หรือไม่.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">ไม่</button>
				<button type="button" class="btn btn-danger" id="btnYes">ใช่</button> </a>
			</div>
		</div>
	</div>
</div>   


<div id="myModal1" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box" style="color: #5cb85c;">
					<i class="material-icons">&#xe5ca;</i>
				</div>				 -->
				<h4 class="modal-title"><p id="success1"></p></h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>			
		</div>
	</div>
</div>   

<!-- Modal -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header"  style="background-color: #d6e9c6;" >
        <h4 class="modal-title"align="center"><p id="title"></p></h4> 
      </div>

      <div class="modal-body">
      <form id="edit_form" method="post">
<table class="table" > 
      <tr>
      <input type="hidden" name="kpi_id" id="kpi_id" /> 
        <td width="180px"> ปีงบประมาณ </td>
        <td> 
        <div style="width: 8.5em;">
        <select name="strategy_year" id="strategy_year" class="form-control"  disabled>
        </div>
        <?php
            $strSQL_year = "SELECT DISTINCT(`strategy_year`) FROM strategy order by strategy_year";
            $objQuery_year = mysql_query($strSQL_year) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_year))
              {
           ?>
            <option value="<?php echo $objResult["strategy_year"];?>" selected><?php echo $objResult["strategy_year"];?></option>
            
        <?php } ?>    
        </select> 
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อยุทธศาสตร์ </td>
        <td> 
				<select name="strategy_name" id="strategy_name" class="form-control"  disabled>
            <?php 
						$Query = "SELECT * FROM strategy";
						$objQuery = mysql_query($Query) or die(mysql_error());
						while($objResult = mysql_fetch_array($objQuery))
						{
						?>
						<option value="<?php echo $objResult["strategy_id"];?>" selected  ><?php echo $objResult["strategy_name"];?></option>
            <?php } ?>    
        </select>
        </td>
      </tr>
      <tr>
      <input type="hidden" name="criterion_id" id="criterion_id">
      <td width="180px"> ชื่อกลยุทธ์ </td>
        <td> 
        <textarea class="form-control" name="mission_name" id="mission_name" rows="2" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อโครงการ/แผนงาน </td>
        <td> 
        <input type="hidden" name="plan_name1" id="plan_name1">
        <textarea class="form-control" name="plan_name" id="plan_name" rows="2" readonly></textarea>
        <input type="hidden" name="plan_name2" id="plan_name2">
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อเป้าหมาย </td>
        <td> 
        <input type="hidden" name="userid" value="<?php echo $user_id ;?>">
        <input type="hidden" name="target_name1" id="target_name1">
        <div style="width: 41.813em;">
        <select name="target_name" id="target_name" class="form-control fstdropdown-select">
            <?php 
						$Query = "SELECT target_id, target_name FROM target ";
						$objQuery = mysql_query($Query) or die(mysql_error());
            while($objResult = mysql_fetch_array($objQuery))
						{
						?>
						<option value="<?php echo $objResult["target_id"];?>" selected title="<?php echo ($objResult["target_name"]) ;?>"><?php echo ($objResult["target_name"]) ;?></option>
            <?php } ?>    
        </select>
            </div>
        </td>
      </tr>
      <tr>
      <td width="180px"> กลุ่มหน่วยงาน </td>
        <td> 
        <select name="dept_group_name" id="dept_group_name" class="form-control" data-live-search="true" >
            <?php 
						$Query = "SELECT * FROM department_group ";
						$objQuery = mysql_query($Query) or die(mysql_error());
						while($objResult = mysql_fetch_array($objQuery))
						{
						?>
						<option value="<?php echo $objResult["dept_group_id"];?>" selected  ><?php echo $objResult["dept_group_name"];?></option>
            <?php } ?>    
        </select>
        </td>
      </tr>
      <tr>
      <td width="180px"> หน่วยงาน </td>
        <td> 
        <input type="hidden" name="dept_name1" id="dept_name1">
        <input type="hidden" name="dept_name" id="dept_name">
        <select name="dept_name2" id="dept_name2" class="form-control" disabled>
            <?php 
						$Query = "SELECT * FROM department ";
						$objQuery = mysql_query($Query) or die(mysql_error());
						while($objResult = mysql_fetch_array($objQuery))
						{
						?>
						<option value="<?php echo $objResult["dept_id"];?>" selected  ><?php echo $objResult["dept_name"];?></option>
            <?php } ?>    
        </select>
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อตัวชี้วัด </td>
        <td> 
        <input type="hidden" name="kpi_name1" id="kpi_name1" class="form-control" >  
        <input type="text" name="kpi_name" id="kpi_name" class="form-control" >  
        </td>
      </tr>
      <tr>
      <td width="181px"> เป้าประสงค์ </td>
        <td> 
        <input type="hidden" name="objective1" id="objective1" placeholder="เป้าประสงค์" class="form-control" >
        <input type="text" name="objective" id="objective" placeholder="เป้าประสงค์" class="form-control" >
        </td>
      </tr>
      <tr>
      <td width="181px"> คำอธิบาย </td>
        <td> 
        <input type="hidden" name="description1" id="description1" placeholder="คำอธิบาย" class="form-control" >
        <input type="text" name="description" id="description" placeholder="คำอธิบาย" class="form-control" >
        </td>
      </tr>
      <tr>
      <td width="181px"> เกณฑ์การให้คะแนน </td>
      <td>
     
      <table class="table table-bordered">
      <tr><td  align="center"> ระดับ</td>
          <td  align="center"> เกณฑ์การให้คะแนน</td></tr>
      <tbody id="p_scents">
        <input type="hidden" name="llevel1" id="llevel1"/><input type="hidden" name="sscore1" id="sscore1" class="form-control" />
        <input type="hidden" name="llevel2" id="llevel2"/><input type="hidden" name="sscore2" id="sscore2" class="form-control" />
        <input type="hidden" name="llevel3" id="llevel3"/><input type="hidden" name="sscore3" id="sscore3" class="form-control" />
        <input type="hidden" name="llevel4" id="llevel4"/><input type="hidden" name="sscore4" id="sscore4" class="form-control" />
        <input type="hidden" name="llevel5" id="llevel5"/><input type="hidden" name="sscore5" id="sscore5" class="form-control" />

        <tr><td align="center"><input type="hidden" name="level1" id="level1" value="1"/>1</td><td><input type="text" name="score1" id="score1" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level2" id="level2" value="2"/>2</td><td><input type="text" name="score2" id="score2" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level3" id="level3" value="3"/>3</td><td><input type="text" name="score3" id="score3" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level4" id="level4" value="4"/>4</td><td><input type="text" name="score4" id="score4" class="form-control" /></td></tr>
        <tr><td align="center"><input type="hidden" name="level5" id="level5" value="5"/>5</td><td><input type="text" name="score5" id="score5" class="form-control" /></td></tr>
    </tbody>
      </table>
      </td>
      </tr>
      <tr>
      <td width="181px"> ได้รับจัดสรร </td>
        <td> 
        <input type="hidden" name="kpi_budgetask1" id="kpi_budgetask1" class="form-control" >
        <input type="text" name="kpi_budgetask" id="kpi_budgetask" class="form-control" >
        </td>
      </tr>
      <tr>
      <td width="181px"> การใช้จ่ายงบประมาณ </td>
        <td> 
        <input type="hidden" name="kpi_budgetgot1" id="kpi_budgetgot1" class="form-control">
        <input type="text" name="kpi_budgetgot" id="kpi_budgetgot" class="form-control">
        </td>
      </tr>
      <tr>
      <td width="181px"> ผลการดำเนินงาน </td>
        <td> 
        <input type="hidden" name="kpi_result1" id="kpi_result1" class="form-control">
        <textarea class="form-control" name="kpi_result" id="kpi_result" rows="3"></textarea>
       
        </td>
      </tr>
      <tr>
      <td width="181px"> ค่าคะแนนที่ได้ </td>
        <td> 
        <input type="hidden" name="kpi_comment1" id="kpi_comment1" class="form-control">
        <textarea class="form-control" name="kpi_comment" id="kpi_comment" rows="3"></textarea>
       
        </td>
      </tr>
      <tr>
      <td width="181px"> คำชี้แจงการปฏิบัติงาน/มาตรการที่ได้ดำเนินการ </td>
        <td> 
        <input type="hidden" name="explanation1" id="explanation1" class="form-control">
        <textarea class="form-control" name="explanation" id="explanation" placeholder="คำชี้แจงการปฏิบัติงาน/มาตรการที่ได้ดำเนินการ" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ปัจจัยสนับสนุนต่อการดำเนินงาน </td>
        <td> 
        <input type="hidden" name="factor1" id="factor1" class="form-control">
        <textarea class="form-control" name="factor" id="factor" placeholder="ปัจจัยสนับสนุนต่อการดำเนินงาน" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> อุปสรรคต่อการดำเนินงาน </td>
        <td> 
        <input type="hidden" name="barrier1" id="barrier1" class="form-control">        
        <textarea class="form-control" name="barrier" id="barrier" placeholder="อุปสรรคต่อการดำเนินงาน" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ข้อเสนอแนะสำหรับการดำเนินงานในปีต่อไป </td>
        <td> 
        <input type="hidden" name="suggestion1" id="suggestion1" class="form-control"> 
        <textarea class="form-control" name="suggestion" id="suggestion" placeholder="ข้อเสนอแนะสำหรับการดำเนินงานในปีต่อไป" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> หลักฐานอ้างอิง </td>
        <td> 
        <input type="hidden" name="reference1" id="reference1" class="form-control">
        <textarea class="form-control" name="reference" id="reference" placeholder="หลักฐานอ้างอิง" rows="4"></textarea>
        </td>
      </tr>
      <tr>
      <td width="181px"> ระยะเวลาดำเนินการ </td>
        <td>
        <input type="hidden" class="form-control" name="time1" placeholder="ระยะเวลาดำเนินการ" id="time1" > 
        <input type="text" class="form-control" name="time" placeholder="ระยะเวลาดำเนินการ" id="time" >
        </td>
      </tr>
      <tr>
      <td width="180px"> หมายเหตุ </td>
        <td> 
        <input type="hidden" name="kpi_remark1" id="kpi_remark1" class="form-control">
        <textarea class="form-control" name="kpi_remark" id="kpi_remark" rows="3" ></textarea>
        </td>
      </tr>
</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
        <input type="submit" class="btn btn-primary" name="edit"  id="edit" value="บันทึก" />
        </form>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function () {
      $('#dept_group_name').on('selector change', function () {
            if ($(this).val() != '') {
               $('#dept_name').prop('disabled', false);
          
            }
            else {
              $('#dept_name').prop('disabled', true);
               
            }
        });
        $('#dept_name').on('selector change', function () {
            if ($(this).val() != '') {
             
            }
            else {
             
            }
        });
       
    });
</script>

    