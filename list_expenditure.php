<link rel="stylesheet" href="css/bootstrap-select.css">
<script src="js/bootstrap-select.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<?php
error_reporting(0);
include("config/config.php");
$user_id = $_SESSION['user_id'];
?>
<script>
        function calPercent() {
        var plan = ["plan_1_oct", "plan_2_nov", "plan_3_dec", "plan_4_jan", "plan_5_feb", "plan_6_mar", "plan_7_apr", "plan_8_may", "plan_9_jun", "plan_10_jul", "plan_11_aug", "plan_12_sep"];
        var act = ["act_1_oct", "act_2_nov", "act_3_dec", "act_4_jan", "act_5_feb", "act_6_mar", "act_7_apr", "act_8_may", "act_9_jun", "act_10_jul", "act_11_aug", "act_12_sep"];
        var sum = ["sum_1_oct", "sum_2_nov", "sum_3_dec", "sum_4_jan", "sum_5_feb", "sum_6_mar", "sum_7_apr", "sum_8_may", "sum_9_jun", "sum_10_jul", "sum_11_aug", "sum_12_sep"];
                     for (var row = 0; row < 12; row++) {

                var a = Number(document.getElementById(plan[row]).value);
                var b = Number(document.getElementById(act[row]).value);
                var c = (b*100)/a;
                if (isNaN(c)) c = 0;{
                    document.getElementById(sum[row]).innerHTML = (c.toFixed(2)+" %");
                }
              
                     }
                     }
      
</script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#txtHint').hide();
                $('#strategy_year').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {year_expenditure: $(this).val()},
                        url: 'ajax/showKpi.php',
                        success: function(data) {
                          $('#kpi_name').html(data).selectpicker('refresh');
                        }
                    });
                    return false;
                });
				$('#kpi_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {kpi_name1: $(this).val()},
                        url: 'ajax/showTransaction.php',
                        dataType:"json",  
                        success: function(data) { 
                            $('#txtHint').fadeIn(); 
                            $('#text_kpi_name').text(data.kpi_name);
                            $('#kpi_id').val(data.kpi_id);
                            $('#transac_id1').val(data.transac_id);
                            $('#transactype_id').val(data.transactype_id);
                            $('#plan_1_oct').val(data.t1_oct);
                            $('#plan_2_nov').val(data.t2_nov);
                            $('#plan_3_dec').val(data.t3_dec);
                            $('#plan_4_jan').val(data.t4_jan);
                            $('#plan_5_feb').val(data.t5_feb);
                            $('#plan_6_mar').val(data.t6_mar);
                            $('#plan_7_apr').val(data.t7_apr);
                            $('#plan_8_may').val(data.t8_may);
                            $('#plan_9_jun').val(data.t9_jun);
                            $('#plan_10_jul').val(data.t10_jul);
                            $('#plan_11_aug').val(data.t11_aug);
                            $('#plan_12_sep').val(data.t12_sep);
                            $('#transac_remark').val(data.transac_remark);
                            $('#plan1_1_oct').val(data.t1_oct);
                            $('#plan1_2_nov').val(data.t2_nov);
                            $('#plan1_3_dec').val(data.t3_dec);
                            $('#plan1_4_jan').val(data.t4_jan);
                            $('#plan1_5_feb').val(data.t5_feb);
                            $('#plan1_6_mar').val(data.t6_mar);
                            $('#plan1_7_apr').val(data.t7_apr);
                            $('#plan1_8_may').val(data.t8_may);
                            $('#plan1_9_jun').val(data.t9_jun);
                            $('#plan1_10_jul').val(data.t10_jul);
                            $('#plan1_11_aug').val(data.t11_aug);
                            $('#plan1_12_sep').val(data.t12_sep);
                            $('#transac_remark1').val(data.transac_remark);
                            
                        }
                    });
                    return false; 
                });
                $('#kpi_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {kpi_name2: $(this).val()},
                        url: 'ajax/showTransaction.php',
                        dataType:"json",  
                        success: function(data) { 
                            $('#transac_id2').val(data.transac_id);
                            $('#act_1_oct').val(data.t1_oct);
                            $('#act_2_nov').val(data.t2_nov);
                            $('#act_3_dec').val(data.t3_dec);
                            $('#act_4_jan').val(data.t4_jan);
                            $('#act_5_feb').val(data.t5_feb);
                            $('#act_6_mar').val(data.t6_mar);
                            $('#act_7_apr').val(data.t7_apr);
                            $('#act_8_may').val(data.t8_may);
                            $('#act_9_jun').val(data.t9_jun);
                            $('#act_10_jul').val(data.t10_jul);
                            $('#act_11_aug').val(data.t11_aug);
                            $('#act_12_sep').val(data.t12_sep);
                            $('#act1_1_oct').val(data.t1_oct);
                            $('#act1_2_nov').val(data.t2_nov);
                            $('#act1_3_dec').val(data.t3_dec);
                            $('#act1_4_jan').val(data.t4_jan);
                            $('#act1_5_feb').val(data.t5_feb);
                            $('#act1_6_mar').val(data.t6_mar);
                            $('#act1_7_apr').val(data.t7_apr);
                            $('#act1_8_may').val(data.t8_may);
                            $('#act1_9_jun').val(data.t9_jun);
                            $('#act1_10_jul').val(data.t10_jul);
                            $('#act1_11_aug').val(data.t11_aug);
                            $('#act1_12_sep').val(data.t12_sep);
                          }
                    });
                    return false;
                    
                });
                
            });

</script>

<center>
<div class="blank">
<div class="container">
<br> <h3 style="color: #68AE00;">รายการแผนงบประมาณ</h3><br> 
<hr>
<div class="thumbnail">
<table class="table table-hover" >
<thead class="text-muted">
<tr>
  <th scope="col">เลือกปีงบประมาณ</th>
  <th scope="col" > 
  <!-- <div style="width: 34.000em;"> -->
  <select name="strategy_year" id="strategy_year"  class="form-control"  required oninvalid="this.setCustomValidity('กรุณาเลือกปีงบประมาณ')"  oninput="setCustomValidity('')" />
        <option value="" selected disabled="disabled">เลือกปีงบประมาณ</option>
        <?php
            $strSQL_year = "SELECT DISTINCT(`strategy_year`) FROM strategy order by strategy_year";
            $objQuery_year = mysql_query($strSQL_year) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_year))
              {
           ?>
            <option value="<?php echo $objResult["strategy_year"];?>"><?php echo $objResult["strategy_year"];?></option>
            
        <?php } ?>    
        </select> </th>
  <!-- </div> -->
</tr>
<tr>
  <th scope="col">เลือกตัวชี้วัด</th>
  <th scope="col" > 
  <div style="position: initial; width: 61.000em;">
  <select name="kpi_name" id="kpi_name"  class="form-control selectpicker" data-live-search="true"  required oninvalid="this.setCustomValidity('กรุณาเลือกตัวชี้วัด')"  oninput="setCustomValidity('')" disabled="disabled">
        
        <option value="" selected >เลือกตัวชี้วัด</option>
            <option value=""></option>
        </select> </th>
  </div>
</tr>

</thead>

</table>
</div> <!-- card.// -->
<div id="txtHint">
<h4 id="text_kpi_name"></h4><br> 
<form name="listexpenditure" action="ajax/edit_transaction.php" method="post" onKeyUp="calPercent()">

<input type="hidden" name="kpi_id" id="kpi_id">
<input type="hidden" name="transactype_id" id="transactype_id">
<input type="hidden" name="userid" value="<?php echo $user_id ;?>">
<input type="hidden" name="transac_id1" id="transac_id1">
<input type="hidden" name="transac_id2" id="transac_id2">
<?php 
$month = array(array("ตุลาคม","1_oct"),array("พฤศจิกายน", "2_nov"),array("ธันวาคม", "3_dec"),array("มกราคม", "4_jan"),
 array("กุมภาพันธ์", "5_feb"),array("มีนาคม", "6_mar"),array("เมษายน", "7_apr"),array("พฤษภาคม", "8_may"),array("มิถุนายน", "9_jun"),
 array("กรกฎาคม", "10_jul"),array("สิงหาคม", "11_aug"),array("กันยายน", "12_sep"));
 
 for ($row = 0; $row < 12; $row++) {
    for ($col = 0; $col < 1; $col++) {
        for ($id = 1; $id < 2; $id++) {
?>
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3><?php  echo $month[$row][$col] ; ?></h3>
      
  </div>    
      <br>
      <table>
          <tr>
            <td align="right">แผน  :  </td>
            <td><input type="number" name="<?php  echo "plan_".$month[$row][$id] ; ?>" id="<?php  echo "plan_".$month[$row][$id] ; ?>" style="text-align: right" style="width: 180px" class="form-control"  /></td>
            <input type="hidden" name="<?php  echo "plan1_".$month[$row][$id] ; ?>" id="<?php  echo "plan1_".$month[$row][$id] ; ?>" />
          </tr>
          <tr>
            <td align="right">ผล  :  </td>
            <td><input type="number" name="<?php  echo "act_".$month[$row][$id] ; ?>" id="<?php  echo "act_".$month[$row][$id] ; ?>" style="text-align: right" style="width: 180px" class="form-control"/></td>
            <input type="hidden" name="<?php  echo "act1_".$month[$row][$id] ; ?>" id="<?php  echo "act1_".$month[$row][$id] ; ?>" />
          </tr>
          <tr>
            <td align="right">คิดเป็น  :</td>
            <td><p align="center" id="<?php  echo "sum_".$month[$row][$id]; ?>"  > </p> </td>
          </tr>
      </table>

     
    </div>
  </div>
</div>
<?php  
    }   
  }
} ?>
        <textarea class="form-control" name="transac_remark" id="transac_remark" placeholder="หมายเหตุ" rows="3"></textarea>
        <input type="hidden" name="transac_remark1" id="transac_remark1" />
 <br> <p><input type="submit" class="btn btn-success" role="button" value="บันทึก"> <input type="reset" class="btn btn-default" role="button" value="ยกเลิก"></p>

</div> 
<!--container end.//-->
</form>
</div>
</center>

<script>
    $(document).ready(function () {
    $('#strategy_year').on('selector change', function () {
            if ($(this).val() != '') {
                $('#kpi_name').prop('disabled', false);
            }
            else {
                $('#kpi_name').prop('disabled', true);
            }
        });
      
    });
</script>

      
