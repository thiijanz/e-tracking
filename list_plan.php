<?php
if(empty($_POST['select_year'])){
  $select_year = date('Y')+543;
}else{$select_year = $_POST['select_year'];}
?>


<link rel="stylesheet" href="css/fstdropdown.css">
<script src="js/fstdropdown.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style type="text/css">
	.modal-confirm {		
		color: #636363;
		width: 400px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
        text-align: center;
		font-size: 14px;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
        position: relative;
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -10px;
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -2px;
	}
	.modal-confirm .modal-body {
		color: #999;
	}
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;		
		border-radius: 5px;
		font-size: 13px;
		padding: 10px 15px 25px;
	}
	.modal-confirm .modal-footer a {
		color: #999;
	}		
	.modal-confirm .icon-box {
		width: 80px;
		height: 80px;
		margin: 0 auto;
		border-radius: 50%;
		z-index: 9;
		text-align: center;
		border: 3px solid ;
	}
	.modal-confirm .icon-box i {
		/* color: #f15e5e; */
		font-size: 46px;
		display: inline-block;
		margin-top: 13px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #60c7c1;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
		min-width: 120px;
        border: none;
		min-height: 40px;
		border-radius: 3px;
		margin: 0 5px;
		outline: none !important;
    }
	.modal-confirm .btn-info {
        background: #c1c1c1;
    }
    .modal-confirm .btn-info:hover, .modal-confirm .btn-info:focus {
        background: #a8a8a8;
    }
    .modal-confirm .btn-danger {
        background: #f15e5e;
    }
    .modal-confirm .btn-danger:hover, .modal-confirm .btn-danger:focus {
        background: #ee3535;
    }
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
</style>

<?php 
include("config/config.php");
?>
<script>
$(document).ready(function() {
  $("#searchplan").on("keypress click input", function() {
    var val = $(this).val();

    if (val.length) {
      $(".portlet-grid .panel-success").hide().filter(function() {
        return $('.panel-heading .panel-title', this).text().toLowerCase().indexOf(val.toLowerCase()) > -1;
      }).show();
    } else {
      $(".portlet-grid .panel-success").show();
    }
  });
});
</script>
<script>
$(document).ready(function() {
		$('#myModal').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var plan_id = button.data('id');
			var name = button.data('name');

			var modal = $(this);
			modal.find('#btnYes').val(plan_id);
			modal.find('#name').text(name);

		});

$(document).on("click", "#btnYes", function () 
{
	$.ajax({
    url:"ajax/deleteRecords.php", 
    type:"POST",
	  data: {plan_id: $(this).val()},
    success: function(data)
	{
		$('#myModal1').modal("show");
		$('#success1').text(data);
    	fetch_data(); 

    },
     error: function(){
       alert("error");
     }  
   });  

    $("#listPlan").load(location.href+" #listPlan>*","");
  	$('#myModal').modal('hide');
	 
 });
});
</script>
<script type="text/javascript">
            $(document).ready(function() {
                $('#mission_name').change(function() {
                    $.ajax({
                        type: 'POST',
                        data: {mission_id: $(this).val()},
                        url: 'ajax/showDataModal.php',
                        dataType:"json",
                        success: function(data) {
                          $('#strategy_year').val(data.strategy_year);
                          $('#strategy_name').val(data.strategy_id); 
													$('#editModal').modal('show');  
                        }
                    });
                    return false;
                });
            });

</script>
<script>
$(document).ready(function() {
	$(document).on('click', '.edit_data', function(){  
           var plan_id = $(this).attr("id");  
           $.ajax({  
                url:"ajax/showDataModal.php",  
                method:"POST",  
                data:{plan_id:plan_id},  
                dataType:"json",  
                success:function(data){  
										 $('#title').text(data.plan_name);
                     $('#plan_id').val(data.plan_id);  
                     $('#mission_name').val(data.mission_id); 
                     $('#strategy_name').val(data.strategy_id);
                     $('#plan_name').val(data.plan_name); 
                     $('#plan_remark').val(data.plan_remark);  
                     $('#mission_id').val(data.mission_id); 
                     $('#plan_name1').val(data.plan_name); 
                     $('#plan_remark1').val(data.plan_remark); 
                     $('#editModal').modal('show');  
                }  
           });  
      });
      $('#edit_form').on("submit", function(event){  
           event.preventDefault();  
                $.ajax({  
                     url:"ajax/edit_plan.php",  
                     method:"POST",  
                     data:$('#edit_form').serialize(),  
                     beforeSend:function(){  
                          $('#edit').val("กำลังบันทึก..");  
                     },  
                     success:function(data){  
                          $('#edit_form')[0].reset();  
                          $('#editModal').modal('hide');
													$('#edit').val("บันทึก");
													$("#listPlan").load(location.href+" #listPlan>*","");  
													$('#myModal1').modal("show");
													$('#success1').text(data);
    											fetch_data();
                          
                     }  
                });  
      });   
	});
</script>

<?php
error_reporting(0);
include("config/config.php");
$user_id = $_SESSION['user_id'];
$strSQL = "SELECT p.plan_id, p.mission_id, m.mission_name, p.plan_name, p.plan_remark , s.strategy_id,s.strategy_year,s.strategy_name 
FROM plan p join mission m on p.mission_id = m.mission_id join strategy s on m.strategy_id  = s.strategy_id WHERE s.strategy_year='".$select_year."'";
$objQuery = mysql_query($strSQL) or die(mysql_error());
$Num_Rows = mysql_num_rows($objQuery);

$i=1;
$Per_Page = 100;   // Per Page

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order  by p.plan_id ASC LIMIT $Page_Start , $Per_Page";
$objQuery  = mysql_query($strSQL);
?>
<center>
<div class="blank">
<h3 style="color: #68AE00;">รายการโครงการ/แผนงาน</h3><br>
<form action="main.php?menu=list_plan" id="select_year" name="select_year" method="POST" >
<div style="color: #68AE00;">ปีงบประมาณ  : 
    
      <select id="select_year" name="select_year" onchange="this.form.submit()" >
          <option value="0">เลือกปีงบประมาณ</option>
<?php
$strSQLyear ="SELECT DISTINCT strategy_year from strategy order by strategy_year asc";
$objQueryyear  = mysql_query($strSQLyear);
while($objyearResult = mysql_fetch_array($objQueryyear))
{?>       <option value="<?php echo $objyearResult['strategy_year'] ; ?>"
          <?php if($select_year==$objyearResult['strategy_year']){echo "selected";} ?> >
          <?php echo $objyearResult['strategy_year'] ; ?> </option>
<?php } ?>
      </select>
</div>
</form>
<br>
<input type="text" name="searchplan" id="searchplan" placeholder="ค้นหาโครงการ/แผนงาน.... " class="form-control" style="width: 450px;">
<br>
<form id="listPlan" method="post">

 <div class="portlet-grid panel-group" >
 <?php
      while($objResult = mysql_fetch_array($objQuery))
      {
    ?>
    <div class="panel panel-success"  id="accordion">
 
      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $objResult["plan_id"];?>" >
        <h4 class="panel-title">
          <a href="#"><?=  (($Per_Page*($Page-1))+$i) ?>. <?php echo $objResult["plan_name"];?></a>
        </h4>
      </div>
      <div id="<?php echo $objResult["plan_id"];?>" class="panel-collapse collapse">
        <div class="panel-body">
<table class="table"> 
      <tr>
        <td width="181px"> ปีงบประมาณ </td>
        <td> 
        <select name="strategy_year" class="form-control"  disabled style="width: 180px">
            <option value="<?php echo $objResult["strategy_year"];?>"><?php echo $objResult["strategy_year"];?></option>
        </select>
        <!-- <input type="text" class="form-control"> -->
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อยุทธศาสตร์ </td>
        <td> 
        <input type="text" name="strategy_name" value="<?php echo $objResult["strategy_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td  width="181px"> ชื่อกลยุทธ์ </td>
        <td> 
        <select name="mission_name"  class="form-control" disabled >
              <option value="<?php echo $objResult["mission_id"];?>"><?php echo $objResult["mission_name"];?></option>
        </select>
        </td>
      </tr>
      <tr>
      <td width="181px"> ชื่อโครงการ/แผนงาน </td>
        <td> 
        <input type="text" name="plan_name" value="<?php echo $objResult["plan_name"];?>" class="form-control" disabled>
        </td>
      </tr>
      <tr>
      <td width="181px"> หมายเหตุ </td>
        <td> 
        <textarea class="form-control" name="plan_remark" value="<?php echo $objResult["plan_remark"];?>" rows="3" disabled></textarea>
        </td>
      </tr>
      <tr>
      <td colspan="2"> 
        <center>
        <a href="#myModal" data-toggle="modal" data-name="<?php echo $objResult["plan_name"];?>" data-id="<?php echo $objResult["plan_id"];?>"> <button class="btn btn-danger"> ลบ </button></a>
        &nbsp; 
				<input type="button" name="edit" value="แก้ไข" id="<?php echo $objResult["plan_id"];?>" class="btn btn-warning edit_data" />
        </center>
      </td>
      </tr>
      <tr>
      <td colspan="2">
      <?php 
        $history = mysql_query("SELECT distinct(plan_log.plan_id) FROM plan_log join plan on plan_log.plan_id = plan.plan_id ");
        // $Rows = mysql_num_rows($history);
        while($resultHis = mysql_fetch_array($history)){
            $hisid =  $resultHis["plan_id"];
            $oldid = $objResult["plan_id"];
            if ($oldid == $hisid) {
      ?>
      <a href="main.php?menu=history_log&plan_id=<?php echo $objResult["plan_id"];?>"  class="view_history"> <h5>ประวัติการแก้ไข</h5></a>
        <?php }
    } ?>
      </td>
      </tr>
</table>

        </div>
      </div>
    </div> 
    <?php $i++; } ?>
</div>
</form>
</div>
      
<br>
ทั้งหมด <?php echo $Num_Rows;?> รายการ : <?php echo $Num_Pages;?> หน้า : &nbsp;

<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?menu=list_plan&Page=$Prev_Page'><button class='btn btn-default'>&laquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled>&laquo;</button> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "<a href='$_SERVER[SCRIPT_NAME]?menu=list_plan&Page=$i'><button class='btn btn-primary'>  $i  </button></a>";
	}
	else
	{
		echo "<button class='btn btn-default' disabled> $i </button>";
	}
}
if($Page!=$Num_Pages)
{
	echo "<a href ='$_SERVER[SCRIPT_NAME]?menu=list_plan&Page=$Next_Page'><button class='btn btn-default'> &raquo;</button></a> ";
}else{
  echo " <button class='btn btn-default' disabled> &raquo;</button> ";
}

?>
</center> 
<!-- Modal HTML -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box" style="color: #f15e5e;">
					<i class="material-icons">&#xE5CD;</i>
				</div>				
				<h4 class="modal-title">คุณแน่ใจไหม ?</h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p>คุณต้องการลบ </p>
				<p style="color: red;" id="name" name="name"> </p>
				<p>ใช่หรือไม่.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">ไม่</button>
				<button type="button" class="btn btn-danger" id="btnYes">ใช่</button> </a>
			</div>
		</div>
	</div>
</div>   

<div id="myModal1" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<!-- <div class="icon-box" style="color: #5cb85c;">
					<i class="material-icons">&#xe5ca;</i>
				</div>				 -->
				<h4 class="modal-title"><p id="success1"></p></h4>	
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			
		</div>
	</div>
</div>   

<!-- Modal -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
    <div class="modal-header"  style="background-color: #d6e9c6;" >
        <h4 class="modal-title"align="center"><p id="title"></p></h4> 
       
      </div>
      <div class="modal-body">
      <form id="edit_form" method="post">
<table class="table"> 
      <tr>
      <input type="hidden" name="userid" value="<?php echo $user_id ;?>">
      <input type="hidden" name="plan_id" id="plan_id" /> 
        <td width="180px"> ปีงบประมาณ </td>
        <td> 
        <div style="width: 8.5em;">
        <select name="strategy_year" id="strategy_year" class="form-control"  disabled>
        </div>
				<?php
            $strSQL_year = "SELECT DISTINCT(`strategy_year`) FROM strategy order by strategy_year";
            $objQuery_year = mysql_query($strSQL_year) or die(mysql_error());
              while($objResult = mysql_fetch_array($objQuery_year))
              {
           ?>
            <option value="<?php echo $objResult["strategy_year"];?>" selected><?php echo $objResult["strategy_year"];?></option>
            
        <?php } ?>    
        </select> 
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อยุทธศาสตร์ </td>
        <td> 
				<select name="strategy_name" id="strategy_name" class="form-control"  disabled/>
            <?php 
						$Query = "SELECT * FROM strategy";
						$objQuery = mysql_query($Query) or die(mysql_error());
						while($objResult = mysql_fetch_array($objQuery))
						{
						?>
						<option value="<?php echo $objResult["strategy_id"];?>" selected  ><?php echo $objResult["strategy_name"];?></option>
            <?php } ?>    
        </select>
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อกลยุทธ์ </td>
        <td> 
        <input type="hidden" name="mission_id" id="mission_id">
        <div style="width: 41.813em;">
        <select name="mission_name" id="mission_name" class="form-control fstdropdown-select" >
            <?php 
						$Query = "SELECT * FROM mission";
						$objQuery = mysql_query($Query) or die(mysql_error());
						while($objResult = mysql_fetch_array($objQuery))
						{
						?>
						<option value="<?php echo $objResult["mission_id"];?>" selected ><?php echo $objResult["mission_name"];?></option>
            <?php } ?>    
        </select>
            </div>
        </td>
      </tr>
      <tr>
      <td width="180px"> ชื่อโครงการ/แผนงาน </td>
        <td> 
        <input type="hidden" name="plan_name1"  id="plan_name1">
        <textarea class="form-control" name="plan_name"  id="plan_name"></textarea>
        </td>
      </tr>
      <tr>
      <td width="180px"> หมายเหตุ </td>
        <td> 
        <input type="hidden" name="plan_remark1"  id="plan_remark1">
        <textarea class="form-control" name="plan_remark"  id="plan_remark" rows="3" ></textarea>
        </td>
      </tr>
      
</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
        <input type="submit" class="btn btn-primary" name="edit"  id="edit" value="บันทึก" disabled/>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function () {
        $('#mission_name').on('selector change', function () {
            if ($(this).val() != '') {
                $('#edit').prop('disabled', false);
            }
            else {
                $('#edit').prop('disabled', true);
            }
        });
        $('#plan_name').on('input change', function () {
            if ($(this).val() != '') {
                $('#edit').prop('disabled', false);
            }
            else {
                $('#edit').prop('disabled', true);
            }
        });
        $('#plan_remark').on('input change', function () {
            if ($(this).val() != '') {
                $('#edit').prop('disabled', false);
            }
            else {
                $('#edit').prop('disabled', true);
            }
        });
    });
</script>