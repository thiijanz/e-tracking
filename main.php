<?php 
session_start();
include("config/config.php");
if($_SESSION['user_id'] == "")
{
  // echo "<script type='text/javascript'>alert('กรุณาเข้าสู่ระบบ !');</script>";
  // echo '<script>window.location="index.php";</script>';
  header("location:index.php");
  exit();
}
$user_id = $_SESSION['user_id'];
$result = mysql_query("SELECT * FROM user JOIN role ON user.role_id = role.role_id where user.user_id = '$user_id'")
 or die(mysql_error());
$row = mysql_fetch_array($result);
// if($_SESSION['role_id'] != "USER")
// {
//   echo "This page for User only!";
//   exit();
// }	

?>
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--static chart-->
<script src="js/Chart.min.js"></script>
<!--//charts-->
<!-- geo chart -->
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src="lib/modernizr/modernizr-custom.js"><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
     <!-- Chartinator  -->
    <script src="js/chartinator.js" ></script>
    <script type="text/javascript">
        jQuery(function ($) {

            var chart3 = $('#geoChart').chartinator({
                tableSel: '.geoChart',

                columns: [{role: 'tooltip', type: 'string'}],
         
                colIndexes: [2],
             
                rows: [
                    ['China - 2015'],
                    ['Colombia - 2015'],
                    ['France - 2015'],
                    ['Italy - 2015'],
                    ['Japan - 2015'],
                    ['Kazakhstan - 2015'],
                    ['Mexico - 2015'],
                    ['Poland - 2015'],
                    ['Russia - 2015'],
                    ['Spain - 2015'],
                    ['Tanzania - 2015'],
                    ['Turkey - 2015']],
              
                ignoreCol: [2],
              
                chartType: 'GeoChart',
              
                chartAspectRatio: 1.5,
             
                chartZoom: 1.75,
             
                chartOffset: [-12,0],
             
                chartOptions: {
                  
                    width: null,
                 
                    backgroundColor: '#fff',
                 
                    datalessRegionColor: '#F5F5F5',
               
                    region: 'world',
                  
                    resolution: 'countries',
                 
                    legend: 'none',

                    colorAxis: {
                       
                        colors: ['#679CCA', '#337AB7']
                    },
                    tooltip: {
                     
                        trigger: 'focus',

                        isHtml: true
                    }
                }

               
            });                       
        });
    </script>
<!--geo chart-->

<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->
</head>
<body>  
<div class="page-container">  
   <div class="left-content">
     <div class="mother-grid-inner">
            <!--header start here-->
        <div class="header-main">
          <div class="header-left">
              <div class="logo-name">
                   <a href="#"> <h1><img src="images/logo_doeb.png" width="155%" height="50%"></h1> 
                  <!--<img id="logo" src="" alt="Logo"/>--> 
                  </a>                
              </div>

              <!--search-box-->
             <!--    <div class="search-box">
                  <form>
                    <input type="text" placeholder="Search..." required=""> 
                    <input type="submit" value="">          
                  </form>
                </div> -->
                <!--//end-search-box-->
              <div class="clearfix"> </div>
             </div>
             <div class="header-right">
              <div class="profile_details_left"><!--notifications of menu start -->
                <ul class="nofitications-dropdown">
                  <li class="dropdown head-dpdn">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
                    <ul class="dropdown-menu">
                      <li>
                        <div class="notification_header">
                          <h3>You have 3 new messages</h3>
                        </div>
                      </li>
                      <li><a href="#">
                         <div class="user_img"><img src="" alt=""></div>
                         <div class="notification_desc">
                        <p>Lorem ipsum dolor</p>
                        <p><span>1 hour ago</span></p>
                        </div>
                         <div class="clearfix"></div> 
                      </a></li>
                      <li class="odd"><a href="#">
                        <div class="user_img"><img src="" alt=""></div>
                         <div class="notification_desc">
                        <p>Lorem ipsum dolor </p>
                        <p><span>1 hour ago</span></p>
                        </div>
                        <div class="clearfix"></div>  
                      </a></li>
                      <li><a href="#">
                         <div class="user_img"><img src="" alt=""></div>
                         <div class="notification_desc">
                        <p>Lorem ipsum dolor</p>
                        <p><span>1 hour ago</span></p>
                        </div>
                         <div class="clearfix"></div> 
                      </a></li>
                      <li>
                        <div class="notification_bottom">
                          <a href="#">See all messages</a>
                        </div> 
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown head-dpdn">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                    <ul class="dropdown-menu">
                      <li>
                        <div class="notification_header">
                          <h3>You have 3 new notification</h3>
                        </div>
                      </li>
                      <li><a href="#">
                        <div class="user_img"><img src="" alt=""></div>
                         <div class="notification_desc">
                        <p>Lorem ipsum dolor</p>
                        <p><span>1 hour ago</span></p>
                        </div>
                        <div class="clearfix"></div>  
                       </a></li>
                       <li class="odd"><a href="#">
                        <div class="user_img"><img src="" alt=""></div>
                         <div class="notification_desc">
                        <p>Lorem ipsum dolor</p>
                        <p><span>1 hour ago</span></p>
                        </div>
                         <div class="clearfix"></div> 
                       </a></li>
                       <li><a href="#">
                        <div class="user_img"><img src="" alt=""></div>
                         <div class="notification_desc">
                        <p>Lorem ipsum dolor</p>
                        <p><span>1 hour ago</span></p>
                        </div>
                         <div class="clearfix"></div> 
                       </a></li>
                       <li>
                        <div class="notification_bottom">
                          <a href="#">See all notifications</a>
                        </div> 
                      </li>
                    </ul>
                  </li> 
                  <li class="dropdown head-dpdn">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">9</span></a>
                    <ul class="dropdown-menu">
                      <li>
                        <div class="notification_header">
                          <h3>You have 8 pending task</h3>
                        </div>
                      </li>
                      <li><a href="#">
                        <div class="task-info">
                          <span class="task-desc">Database update</span><span class="percentage">40%</span>
                          <div class="clearfix"></div>  
                        </div>
                        <div class="progress progress-striped active">
                          <div class="bar yellow" style="width:40%;"></div>
                        </div>
                      </a></li>
                      <li><a href="#">
                        <div class="task-info">
                          <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                           <div class="clearfix"></div> 
                        </div>
                        <div class="progress progress-striped active">
                           <div class="bar green" style="width:90%;"></div>
                        </div>
                      </a></li>
                      <li><a href="#">
                        <div class="task-info">
                          <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                          <div class="clearfix"></div>  
                        </div>
                         <div class="progress progress-striped active">
                           <div class="bar red" style="width: 33%;"></div>
                        </div>
                      </a></li>
                      <li><a href="#">
                        <div class="task-info">
                          <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                           <div class="clearfix"></div> 
                        </div>
                        <div class="progress progress-striped active">
                           <div class="bar  blue" style="width: 80%;"></div>
                        </div>
                      </a></li>
                      <li>
                        <div class="notification_bottom">
                          <a href="#">See all pending tasks</a>
                        </div> 
                      </li>
                    </ul>
                  </li> 
                </ul>
                <div class="clearfix"> </div>
              </div>
              <!--notification menu end -->
              <div class="profile_details">   
                <ul>
                  <li class="dropdown profile_details_drop">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <div class="profile_img"> 
                        <span class="prfil-img"><img src="" alt=""> </span> 
                        <div class="user-name">
                          <p><?php echo $row["user_name"] ;?></p>
                          <span><?php echo $row["role_name"] ;?></span>
                        </div>
                        <i class="fa fa-angle-down lnr"></i>
                        <i class="fa fa-angle-up lnr"></i>
                        <div class="clearfix"></div>  
                      </div>  
                    </a>
                    <ul class="dropdown-menu drp-mnu">
                      <li> <a href="#"><i class="fa fa-cog"></i> ตั้งค่า</a> </li> 
                      <li> <a href="#"><i class="fa fa-user"></i> โปรไฟล์</a> </li> 
                      <li> <a href="logout.php"><i class="fa fa-sign-out"></i> ออกจากระบบ</a> </li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="clearfix"> </div>       
            </div>
             <div class="clearfix"> </div>  
        </div>
<!--heder end here-->
<!-- script-for sticky-nav -->
    <script>
    $(document).ready(function() {
       var navoffeset=$(".header-main").offset().top;
       $(window).scroll(function(){
        var scrollpos=$(window).scrollTop(); 
        if(scrollpos >=navoffeset){
          $(".header-main").addClass("fixed");
        }else{
          $(".header-main").removeClass("fixed");
        }
       });
       
    });
    </script>
    <!-- /script-for sticky-nav -->
<!--inner block start here-->
<div class="inner-block">
<?php 
		switch ($_GET["menu"]) {
		case "dashboard":
			include("main_dashboard.php");
			break;
		case "strategy":
			include("create_strategy.php");
      break;
    case "mission":
			include("create_mission.php");
      break;
    case "plan":
			include("create_plan.php");
      break;  
    case "target":
			include("create_target.php");
      break; 
    case "kpi":
			include("create_Kpi.php");
      break;
    case "transaction":
      include("create_transaction.php");
      break;
    case "expenditure":
			include("create_transaction.php");
			break;
		case "report":
			include("report.php");
      break;
    case "list_strategy":
			include("list_strategy.php");
      break;
    case "list_mission":
			include("list_mission.php");
      break;
    case "list_plan":
			include("list_plan.php");
      break;
    case "list_target":
			include("list_target.php");
      break;
    case "list_kpi":
			include("list_Kpi.php");
      break;
    case "list_transaction":
			include("list_transaction.php");
      break;
    case "list_expenditure":
			include("list_expenditure.php");
      break;
    case "history_log":
			include("history_log.php");
      break;
		default:
			include("main_dashboard.php");
		}
    ?>
<!--climate end here-->
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
   <p>© สงวนลิขสิทธิ์พุทธศักราช 2562 กรมธุรกิจพลังงาน กระทรวงพลังงาน </p>
</div>  
<!--COPY rights end here-->
</div>
</div>
<!--slider menu-->
    <div class="sidebar-menu">
        <div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
            <!--<img id="logo" src="" alt="Logo"/>--> 
        </a> </div>     
        <br>  <br>  <br> 
        <div class="menu">
        <ul id="menu" >
          <?php if($_SESSION["role_id"] == '2') //เจ้าหน้าที่ กผพ.
		        { ?>
            <li id="menu-home"><a href="main.php?menu=dashboard"><i class="fa fa-tachometer"></i><span>หน้าหลัก</span></a></li>
            
            </li>  
            <li><a href="#"><i class="fa fa-file-text"></i><span>ยุทธศาสตร์</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul>
                  <li><a href="main.php?menu=strategy">สร้างยุทธศาสตร์</a></li>
                  <li><a href="main.php?menu=list_strategy">รายการยุทธศาสตร์</a></li>
              </ul>
              </li>
              <li><a href="#"><i class="fa fa-calendar"></i><span>กลยุทธ์</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                <li><a href="main.php?menu=mission">สร้างกลยุทธ์</a></li>
                <li><a href="main.php?menu=list_mission">รายการกลยุทธ์</a></li>
                </ul>   
              </li>
              <li><a href="#"><i class="fa fa-list-alt"></i><span>โครงการ/แผนงาน</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                <li><a href="main.php?menu=plan">สร้างโครงการ/แผนงาน</a></li>  
                <li><a href="main.php?menu=list_plan">รายการโครงการ/แผนงาน</a></li> 
                </ul>
              </li>
              <li><a href="#"><i class="fa fa-bullseye"></i><span>เป้าหมาย</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>  
                <li><a href="main.php?menu=target">สร้างเป้าหมาย</a></li> 
                <li><a href="main.php?menu=list_target">รายการเป้าหมาย</a></li> 
                </ul>
              </li>
              <li><a href="#"><i class="fa fa-line-chart"></i><span>ตัวชี้วัด</span><span class="fa fa-angle-right" style="float: right"></span></a>
                <ul>
                <li><a href="main.php?menu=kpi">สร้างตัวชี้วัด</a></li> 
                <li><a href="main.php?menu=list_kpi">รายการตัวชี้วัด</a></li>                   
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-bar-chart"></i><span>ข้อมูลดำเนินงาน</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul>
              <li><a href="main.php?menu=transaction">แผนปฏิบัติงาน</a></li>
                <li><a href="main.php?menu=list_transaction">รายการแผนปฏิบัติงาน</a></li>
                <li><a href="main.php?menu=expenditure">แผนงบประมาณ</a></li>
                <li><a href="main.php?menu=list_expenditure">รายการแผนงบประมาณ</a></li>                     
              </ul>
            </li>
            <li id="menu-comunicacao" ><a href="#"><i class="fa fa-book nav_icon"></i><span>รายงาน</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-comunicacao-sub" >
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานตามยุทธศาสตร์ / แผนงาน /ต ัวชี้วัด</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานรายไตรมาส</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานแจ้งเตือนไม่ได้ตามตัวชี้วัด</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานสรุปส่วนกลาง / ส่วนภูมิภาค</a></li>
              </ul>
            </li>

          <?php } else if($_SESSION["role_id"]== '3' || $_SESSION["role_id"]== '4') { //ส่วนกลาง/สพภ.
		         ?>
            <li id="menu-home"><a href="main.php?menu=dashboard"><i class="fa fa-tachometer"></i><span>หน้าหลัก</span></a></li>
            <li><a href="#"><i class="fa fa-bar-chart"></i><span>ข้อมูลดำเนินงาน</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul>
              <li><a href="main.php?menu=transaction">แผนปฏิบัติงาน</a></li>
                <li><a href="main.php?menu=list_transaction">รายการแผนปฏิบัติงาน</a></li>
                <li><a href="main.php?menu=expenditure">แผนงบประมาณ</a></li>
                <li><a href="main.php?menu=list_expenditure">รายการแผนงบประมาณ</a></li>               
              </ul>
            </li>
            <li id="menu-comunicacao" ><a href="#"><i class="fa fa-book nav_icon"></i><span>รายงาน</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-comunicacao-sub" >
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานตามยุทธศาสตร์ / แผนงาน /ต ัวชี้วัด</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานรายไตรมาส</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานแจ้งเตือนไม่ได้ตามตัวชี้วัด</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานสรุปส่วนกลาง / ส่วนภูมิภาค</a></li>
              </ul>
            </li>
            <?php 
            }else if($_SESSION["role_id"]== '5') { // ผู้บริหาร 
            ?>
            <li id="menu-comunicacao" ><a href="#"><i class="fa fa-book nav_icon"></i><span>รายงาน</span><span class="fa fa-angle-right" style="float: right"></span></a>
              <ul id="menu-comunicacao-sub" >
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานตามยุทธศาสตร์ / แผนงาน /ต ัวชี้วัด</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานรายไตรมาส</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานแจ้งเตือนไม่ได้ตามตัวชี้วัด</a></li>
                <li id="menu-mensagens" ><a href="main.php?menu=report">รายงานสรุปส่วนกลาง / ส่วนภูมิภาค</a></li>
              </ul>
            </li>
            <?php } ?>
          </ul>
        </div>
   </div>
  <div class="clearfix"> </div>
</div>
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="js/scripts.js"></script>
    <!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<!-- mother grid end here-->
</body>
</html>                     